---
title: "File Globbing in Linux"
date: 2020-02-26T12:22:40+06:00
image: images/blog/0009.jpg
author: Derek Taylor
---

File globbing refers to "global" patterns that specify sets of filenames with wildcard characters.  Globbing is the * and ? and some other pattern matchers you may be familiar with.  An example would be doing something like the following in the Bash shell: 
`cp *.txt /Documents/text/`

The above command moves all files that end in ".txt" from the current directory to the directory "/Documents/text/".  The `*` is a wildcard that stands for "any string of characters".  `*.txt` is a glob pattern.  When the shell sees a glob, it will perform pathname expansion and replace the glob with matching filenames when it invokes the program. 

&nbsp;
## Globbing Syntax

| Wildcard  |  Description                                         | 
|-----------|------------------------------------------------------|
| `*`       | matches any number of any characters including none  | 
| `?`       | matches any single character                         | 
| `[abc]`   | matches one character given in the bracket           | 
| `[a-z]`   | matches one character from the range given           | 
| `[!abc]`  | matches one character not given in the bracket       | 
| `[!a-z]`  | matches one character not from the range given       | 
&nbsp;

| Example        |  Matches              | 
|----------------|-----------------------|
|  `Bar*`        |  Bar, Bars, Barter    | 
|  `?at`         |  Bat, bat, Cat, cat   | 
|  `[CB]at`      |  Cat or Bat           | 
|  `File[0-9]`   |  File1, File2, etc.   | 
|  `[!B]at`      |  bat, cat, Cat        | 
|  `File[!4-9]`  |  File1, File2, File3  | 
&nbsp;

| Example        |  Does Not Match       |
|----------------|-----------------------|
|  `Bar*`        |  FooBar, Ba, ar       |
|  `?at`         |  at                   |
|  `[CB]at`      |  cat or bat           |
|  `File[0-9]`   |  File, Files, File 10 |
|  `[!B]at`      |  Bat                  |
|  `File[!4-9]`  |  File4, File5, Filexx |
&nbsp;
## Globbing in SQL

SQL has the equivalent of `*` and `?`.  In SQL `%` equates to `*` and `_` equates to `?`.  There is no equivalent to `[...]`.


&nbsp;
## Globs Are Not Regular Expressions

Though they rememble regex, glob patterns are not regular expressions.  Unlike regular expressions, globbing is specifically for pattern matching filenames.  Regex has much more syntax to it and is much more complicated.  Also, the syntax that is shared with globbing is not the same thing in regex.
