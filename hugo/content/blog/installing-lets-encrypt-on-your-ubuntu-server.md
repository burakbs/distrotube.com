---
title: "Installing Let's Encrypt On Your Ubuntu Server"
date: 2020-01-04T13:22:40+06:00
image: images/blog/0004.jpg
author: Derek Taylor
---

#### Downloading Let's Encrypt

Log into your server.  At the command line, let's update our system before installing any software:

+ `apt update`
+ `apt upgrade`


<br />
Then install git if it's not already installed.

+ `apt install`

<br />
Download a clone of Let's Encrypt from their GitHub repository. It is standard practice to install third-party packages to /opt, so you should place it there.

+ `git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt`

<br />
Navigate to the /opt/letsencrypt directory that was created.

+ `cd /opt/letsencrypt`

<br />
#### Creating The SSL Certificate

<br />
Run Let's Encrypt for your domain requiring the certificate.  Note that we did this for two domains--one with the www prefix and one without.

+ `./letsencrypt-auto certonly --standalone -d example.com -d www.example.com`

<br />
You will be asked for an email address to regain control of a lost certificate and receive urgent security notices.  Agree to the Terms of Service.  You may be asked if you wish to share your email address with the Electronic Frontier Foundation (feel free to say "no" if you wish).

<br />
If everything worked properly, you should receive a message similar to the following:

+ `IMPORTANT NOTES:`
+ `- If you lose your account credentials, you can recover them through`
+ `  e-mails sent to somebody@example.com.`
+ `- Congratulations! Your certificate and chain have been saved at`
+ `  /etc/letsencrypt/live/example.com/fullchain.pem. Your`
+ `  cert will expire on 2020-03-31. To obtain a new version of the`
+ `  certificate in the future, simply run Let's Encrypt again.`
+ `- Your account credentials have been saved in your Let's Encrypt`
+ `  configuration directory at /etc/letsencrypt. You should make a`
+ `  secure backup of this folder now. This configuration directory will`
+ `  also contain certificates and private keys obtained by Let's`
+ `  Encrypt, so making regular backups of this folder is ideal.`
+ `- If you like Let's Encrypt, please consider supporting our work by`
+ `  Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate`
+ `  Donating to EFF:                    https://eff.org/donate-le`


<br />
#### You may want to use Certbot with Let's Encrypt

<br />
Certbot (https://certbot.eff.org/) is a free, open source software tool for automatically using Let’s Encrypt certificates. One of the neat things about Certbot is that when you create your certificate using this tool, it automatically creates a cron job for renewing your certificate.  How sweet is that!  The Certbot tool is made by the EFF.



