---
title: "Unfettered Freedom, Ep. 7 - Nvidia Buys ARM, GNOME CoC, Linux Exploits, Free vs Proprietary" 
image: images/thumbs/0715.jpg
date: 2020-09-16T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+7.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
zv2:22 - Nvidia is buying ARM.
+ 8:24 - GNOME's racist Code of Conduct has changed...slightly.
+ 16:54 - Why are some FSF-approved licenses "permissive"?  
+ 22:47 - Linux systems increasingly under attack.
+ 29:20 - Free software versus proprietary software, and how you make your choices.
+ 33:21 - Unfettered Freedom will now be released bi-weekly instead of weekly.
+ 35:31 - Outro and a THANK YOU to the patrons!