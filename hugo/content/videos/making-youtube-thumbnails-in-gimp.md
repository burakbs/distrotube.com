---
title: "Making YouTube Thumbnails In Gimp"
image: images/thumbs/0516.jpg
date: 2020-01-16T12:22:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "GIMP"]
---

#### VIDEO

{{< amazon src="Making+YouTube+Thumbnails+In+Gimp.mp4" >}}
&nbsp;

#### SHOW NOTES

Gimp is a cross-platform image editor available for GNU/Linux, OSX and Windows.  It is free and open source software, and it is what I use to create the artwork for my YouTube channel.

REFERENCED:
+ https://www.gimp.org/
