---
title: "Inside The Making Of DistroTube"
image: images/thumbs/0303.jpg
date: Sat, 03 Nov 2018 00:11:41 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Inside+The+Making+Of+DistroTube.mp4" >}}
&nbsp;

#### SHOW NOTES

A quick look at the hardware and software that I use to make the content for this YouTube channel. 
