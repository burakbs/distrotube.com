---
title: "Chat With Patrons (November 25, 2018)"
image: images/thumbs/0488.jpg
date: Sun, 25 Nov 2018 19:02:44 +0000
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(November+25%2C+2018)-pU5mgaZDEFg.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be a special event for my Patreon supporters. This will be a Zoom video chat just for my patrons. The link to the video chat call will be posted on my Patreon page a few minutes prior to the stream.
