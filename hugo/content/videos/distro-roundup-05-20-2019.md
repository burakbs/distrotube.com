---
title: "Distro Roundup (May 20, 2019) - OpenIndiana, OpenMandriva, ArcoLinux, Peppermint, RoboLinux"
image: images/thumbs/0397.jpg
date: Sun, 19 May 2019 14:40:21 +0000
author: Derek Taylor
tags: ["Distro Reviews", "OpenIndiana", "OpenMandriva", "ArcoLinux", "Peppermint", "RoboLinux"]
---

#### VIDEO

{{< amazon src="Distro+Roundup+May+20+2019.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Distro Roundup:
0:17 OpenIndiana 2019.04
+ 6:44 OpenMandriva LX 4.0 RC
+ 14:12 ArcoLinux 19.05.2
+ 22:08 Peppermint 10
+ 30:09 RoboLinux 10.5


&nbsp;
#### REFERENCED:
+ https://www.openindiana.org/2019/05/12/openindiana-hipster-2019-04-is-here/
+ https://www.openmandriva.org/en/news/article/openmandriva-lx-4-0-rc-released
+ https://arcolinux.info/arcolinux-d-b-19-5/
+ https://peppermintos.com/2019/05/peppermint-10-released/
+ https://robolinux.org/downloads/v10.5-details.html
