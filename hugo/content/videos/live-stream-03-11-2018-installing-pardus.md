---
title: "Live Mar 11, 2018 - Installing Pardus 17.2 Deepin in Virtualbox"
image: images/thumbs/0141.jpg
date: Sun, 11 Mar 2018 21:30:58 +0000
author: Derek Taylor
tags: ["Live Stream", "Distro Reviews", "Pardus"]
---

#### VIDEO

{{< amazon src="Live+Mar+11%2C+2018+-+Installing+Pardus+17.2+Deepin+in+Virtualbox.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this live stream, I think I'm going to install Pardus 17.2 (Deepin) in a virtual machine to check it out. Have never used Pardus. I've heard about it many years ago. I know it was based on Gentoo many years ago, but now it is Debian-based. Let's see if I can get it installed to take a look at it. 
