---
title: "Linux Mint 18.3 Sylvia Install & Review"
image: images/thumbs/0043.jpg
date: Tue, 28 Nov 2017 14:29:56 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Linux Mint", "Cinnamon"]
---

#### VIDEO

{{< amazon src="Linux+Mint+18.3+Sylvia+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today,, I'm installing and reviewing the recently released Linux Mint 18.3 "Sylvia". I'm going to be using the Cinnamon edition of Linux Mint in this review. Sylvia sports a Software Manager with an updated look and feel. Sylvia also comes with Flatpak support out of the box.
