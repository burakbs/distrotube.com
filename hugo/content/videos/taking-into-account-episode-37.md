---
title: "Taking Into Account, Ep. 37 - Linux Troubles, Mint Devs Unhappy, China Bans Crypto, MS Linux Apps"
image: images/thumbs/0380.jpg
date: Thu, 11 Apr 2019 02:48:55 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+37.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=51s">0:51 Is the Linux desktop in trouble? Linus sees ChromeOS and Android as the future of Linux. 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=692s">11:32 Linux Mint's devs are not happy. A rare glimpse into the personal struggles developers face. 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=1147s">19:07 Had enough of Windows 10’s hassles?  One author shares his reasons to abandon Win for Linux! 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=1667s">27:47 China could soon ban cryptomining citing waste of resources.  China being China, or are they right? 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=1920s">32:00 Microsoft in the Linux news.  MS releases VS Code as a snap, and MS doesn't rule out Edge on Linux. 

<a href="https://www.youtube.com/watch?v=0m9teYPlz00&amp;t=2244s">37:24  On a recent video, I was rather harsh regarding Linus Tech Tips and  their Linux coverage.  But a recent video of theirs deserves props! 

📰 REFERENCED:

 <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fthe-linux-desktop-is-in-trouble%2F&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://www.zdnet.com/article/the-lin...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F04%2F08%2Flinux-mint-sobering-update-developer-struggles-community%2F&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Fwww.slashgear.com%2Freasons-to-abandon-windows-for-linux-06572307%2F&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://www.slashgear.com/reasons-to-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Ffossbytes.com%2Fchina-could-soon-ban-cryptocurrency-mining-citing-wastage-of-resources%2F&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://fossbytes.com/china-could-soo...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Fventurebeat.com%2F2019%2F04%2F04%2Fmicrosoft-brings-visual-studio-code-to-linux-as-a-snap%2F&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://venturebeat.com/2019/04/04/mi...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=rjkK9dkdLYdXaQk0JwIx-7dLdCZ8MTU1NzE5NzM0MkAxNTU3MTEwOTQy&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F04%2Fmicrosoft-edge-may-come-to-linux-eventually-just-not-right-now&amp;v=0m9teYPlz00&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/04/m...

<a href="https://www.youtube.com/watch?v=Co6FePZoNgE">https://www.youtube.com/watch?v=Co6Fe...
