---
title: "Live Q&A Plus Maybe Reaching 10K"
image: images/thumbs/0264.jpg
date: Tue, 24 Jul 2018 19:26:19 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Live+Q%26A+Plus+Maybe+Reaching+10K.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight's live stream is just me and you guys.  You got questions.   Maybe I got answers.  Plus the channel approaches a major milestone! 

NEWS REFERENCED IN THIS VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2018%2F07%2Fkde-plasma-5-14-wallpaper-cluster&amp;redir_token=dlGZyrcm2KiRG0Sj0EAJ8dAvsXB8MTU1MzQ1NTYxNUAxNTUzMzY5MjE1&amp;v=lwAZW8qr4Q0&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2018/07/k...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2018%2F07%2Fyaru-ubuntu-theme&amp;redir_token=dlGZyrcm2KiRG0Sj0EAJ8dAvsXB8MTU1MzQ1NTYxNUAxNTUzMzY5MjE1&amp;v=lwAZW8qr4Q0&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2018/07/y...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fkevq.uk%2Fthe-laboriousness-of-lightweight-linux%2F&amp;redir_token=dlGZyrcm2KiRG0Sj0EAJ8dAvsXB8MTU1MzQ1NTYxNUAxNTUzMzY5MjE1&amp;v=lwAZW8qr4Q0&amp;event=video_description" target="_blank">https://kevq.uk/the-laboriousness-of-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.linuxquestions.org%2Fquestions%2Fslackware-14%2Fdonating-to-slackware-4175634729%2F%23post5882751&amp;redir_token=dlGZyrcm2KiRG0Sj0EAJ8dAvsXB8MTU1MzQ1NTYxNUAxNTUzMzY5MjE1&amp;v=lwAZW8qr4Q0&amp;event=video_description" target="_blank">https://www.linuxquestions.org/questi...
