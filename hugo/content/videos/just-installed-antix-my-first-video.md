---
title: "Just Installed AntiX 17.1 - My First Video"
image: images/thumbs/0151.jpg
date: Tue, 20 Mar 2018 21:44:23 +0000
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

{{< amazon src="Just+Installed+AntiX+17.1+-+My+First+Video.mp4" >}}  
&nbsp;

#### SHOW NOTES

Just a quick video from my freshly installed AntiX 17.1. The install was a breeze. I haven't done much in the way of configuring. Took a few hours to import all my backed up files. Installed the basic programs I need to make the videos for this channel I'll check back in later this evening for a more in-depth look at AntiX (will do a live stream). 
