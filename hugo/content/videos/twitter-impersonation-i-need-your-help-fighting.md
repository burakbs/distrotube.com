---
title: "Twitter Impersonation - I Need Your Help Fighting Impersonator And Twitter"
image: images/thumbs/0319.jpg
date: Thu, 20 Dec 2018 19:35:02 +0000
author: Derek Taylor
tags: ["Social Media", ""]
---

#### VIDEO

{{< amazon src="Twitter+Impersonation+-+I+Need+Your+Help+Fighting+Impersonator+And+Twitter.mp4" >}}
&nbsp;

#### SHOW NOTES

Eight months ago, I deleted Twitter from my life.  Or I thought I did.   Apparently, someone on Twitter is impersonating me.  I want Twitter to  take down the offending account but Twitter wants me to send them a copy  of my driver's license or other government ID to prove that I'm me.   Really, Twitter? Help me fight this impersonator.  

Please report the @distrotube account  at: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fhelp.twitter.com%2Fforms%2Fimpersonation&amp;v=tOA_DUWmOqo&amp;redir_token=SDcQaj9Krzcv9zfmjofWFN5dJG58MTU1MzU0MjUyNEAxNTUzNDU2MTI0&amp;event=video_description" target="_blank">https://help.twitter.com/forms/impers... Also, let everyone know that <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ftwitter.com%2Fdistrotube&amp;v=tOA_DUWmOqo&amp;redir_token=SDcQaj9Krzcv9zfmjofWFN5dJG58MTU1MzU0MjUyNEAxNTUzNDU2MTI0&amp;event=video_description" target="_blank">https://twitter.com/distrotube  is not me, especially those following the fake account.  I no longer  have a Twitter account so I need your help (those of you still on  Twitter). Thanks, guys!
