---
title: "Condres OS 19.09 Installation and First Look"
image: images/thumbs/0437.jpg
date: Thu, 05 Sep 2019 23:37:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Condres OS"]
---

#### VIDEO

{{< amazon src="Condres+OS+1909+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I am taking a quick look at Condres OS, an Arch-based Linux distro that features a number of desktop editions. I'm looking at their KDE Plasma edition. Will it "wow" me? Or is it just another Arch spin?


&nbsp;
#### REFERENCED: 
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=uMEcCzYoOtdZAGpU1Pxv5SDRFPh8MTU3NzQ4OTkyNkAxNTc3NDAzNTI2&amp;q=https%3A%2F%2Fcondresos.codelinsoft.it%2F&amp;v=eRA4FqbblCo&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://condresos.codelinsoft.it/
