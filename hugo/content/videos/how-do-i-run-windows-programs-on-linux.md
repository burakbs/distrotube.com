---
title: "How Do I Run My Windows Programs on Linux?"
image: images/thumbs/0105.jpg
date: Sat, 03 Feb 2018 01:40:50 +0000
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="How+Do+I+Run+My+Windows+Programs+on+Linux.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick video discussing the most often asked question I get in feedback from viewers: "How can I get my Windows programs to run on Linux?" I have five possible answers to this question that I rank from best to worst. 
