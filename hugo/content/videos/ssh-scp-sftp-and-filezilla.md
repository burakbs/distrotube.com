---
title: "SSH, SCP, SFTP and FileZilla"
image: images/thumbs/0277.jpg
date: Mon, 17 Sep 2018 18:52:01 +0000
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="SSH%2C+SCP%2C+SFTP+and+FileZilla.mp4" >}}
&nbsp;

#### SHOW NOTES

How can you login to a remote machine to update it or do maintenance?   How can you exchange data between two machines securely?  In this video,  I'm going to briefly discuss SSH and some related tools. NOTE: This video was available for early access viewing for my  subscribers on Patreon.  Thanks for your support! 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%2FSecure_Shell&amp;redir_token=I0ZiHPwwB3zvRw-fiTAXVHtoZZ18MTU1MzQ1MzU3MEAxNTUzMzY3MTcw&amp;v=eQJX9DlKHy0&amp;event=video_description" target="_blank">https://wiki.archlinux.org/index.php/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%2FSSH_keys&amp;redir_token=I0ZiHPwwB3zvRw-fiTAXVHtoZZ18MTU1MzQ1MzU3MEAxNTUzMzY3MTcw&amp;v=eQJX9DlKHy0&amp;event=video_description" target="_blank">https://wiki.archlinux.org/index.php/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%2FSCP_and_SFTP&amp;redir_token=I0ZiHPwwB3zvRw-fiTAXVHtoZZ18MTU1MzQ1MzU3MEAxNTUzMzY3MTcw&amp;v=eQJX9DlKHy0&amp;event=video_description" target="_blank">https://wiki.archlinux.org/index.php/...
