---
title: "Solus 4.0 'Fortitude' - The Budgie Edition - Installation and First Look"
image: images/thumbs/0369.jpg
date: Mon, 18 Mar 2019 23:18:22 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Solus", "Budgie"]
---

#### VIDEO

{{< amazon src="Solus+40+Fortitude+The+Budgie+Edition.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at one of the most anticipated releases EVER!   The long wait is finally over.  Solus 4.0 has been released.  Today I'm  doing a quick installation and first look of Solus 4.0 with Budgie. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=TFtaEX48fo4&amp;q=https%3A%2F%2Fgetsol.us%2F2019%2F03%2F17%2Fsolus-4-released%2F&amp;redir_token=y2OCNWqA1A5JzAOd5Nw4d3n9uSJ8MTU1MzY0MjMwNkAxNTUzNTU1OTA2" target="_blank">https://getsol.us/2019/03/17/solus-4-...
