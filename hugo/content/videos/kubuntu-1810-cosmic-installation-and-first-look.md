---
title: "Kubuntu 18.10 'Cosmic Cuttlefish' Installation and First Look"
image: images/thumbs/0296.jpg
date: Tue, 16 Oct 2018 23:55:00 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Kubuntu"]
---

#### VIDEO

{{< amazon src="Kubuntu+1810+Cosmic+Cuttlefish+Installation+and+First+Look.webm" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Kubuntu 18.10 "Cosmic Cuttlefish".  
Kubuntu features the Plasma 5.13 desktop environment and nice suite of 
programs.  Kubuntu 18.10 will be released "officially" on Thursday so 
technically this daily build that I'm looking at is beta, but nothing 
major should change between now and Thursday.

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Fkubuntu.org%2F&amp;v=D-QpuIHsnmQ&amp;redir_token=arA3HBJzZ-UJvAekEuS4B6mCoQ58MTU1MzQ3MTcyOUAxNTUzMzg1MzI5&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://kubuntu.org/
