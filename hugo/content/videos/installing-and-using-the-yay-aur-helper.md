---
title: "Installing And Using The Yay AUR Helper"
image: images/thumbs/0546.jpg
date: 2020-02-16T12:22:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Installing+And+Using+The+Yay+AUR+Helper.mp4" >}}
&nbsp;

#### SHOW NOTES

Yay is yet another AUR helper (officially, Yet Another Yaourt) written in Go.  It is fast and it supports much of the same syntax that you already use in Pacman.  

REFERENCED:
+  https://wiki.archlinux.org/index.php/Arch_User_Repository
+  https://wiki.archlinux.org/index.php/AUR_helpers