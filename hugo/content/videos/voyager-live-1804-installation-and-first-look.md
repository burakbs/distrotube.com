---
title: "Voyager Live 18.04 Installation and First Look"
image: images/thumbs/0213.jpg
date: Tue, 22 May 2018 23:30:51 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Voyager Live"]
---

#### VIDEO

{{< amazon src="Voyager+Live+18.04+Installation+and+First+Look-1.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at Voyager Live 18.04, a Xubuntu-based Linux distribution that features the XFCE, the plank dock, conky and a bunch of cool wallpapers. https://voyagerlive.org/
