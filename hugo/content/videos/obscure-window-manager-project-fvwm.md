---
title: "Obscure Window Manager Project - FVWM"
image: images/thumbs/0103.jpg
date: Fri, 02 Feb 2018 01:34:54 +0000
author: Derek Taylor
tags: ["FVWM", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+FVWM.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a look at FVWM. This may prove to be one of the most difficult window managers to review because it is so configurable and customizable, and there are no rigid or strict features. You build FVWM into the desktop that you envision. Not sure where I was going with this FVWM config but hopefully it provides some information (and entertainment). REFERENCES: Config I found on Github and modified on this video: <a href="https://github.com/liuxueyang/fvwm-config">https://github.com/liuxueyang/fvwm-config</a>FVWM Beginners Guide     The completed config files can be found on my Github page: <a href="https://gitlab.com/dwt1/dotfiles/tree/master/.fvwm">https://gitlab.com/dwt1/dotfiles/tree/master/.fvwm</a>
