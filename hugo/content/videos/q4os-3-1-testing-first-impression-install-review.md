---
title: "Q4OS 3.1 Testing First Impression Install & Review"
image: images/thumbs/0106.jpg
date: Sun, 04 Feb 2018 01:41:56 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Q4OS", "Trinity"]
---

#### VIDEO

{{< amazon src="Q4OS+3+1+Testing+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick look at Q4OS 3.1 Testing, a Debian-based (Debian Testing) Linux distro that uses the Trinity desktop environment. Q4OS prides itself on being very lighweight. Offers easy-to-use setup wizards to get things like Google Chrome and Virtualbox installed on your system. Due to the low hardware requirements, Q4OS is perfect for older machines. <a href="https://q4os.org/">https://q4os.org/</a>
