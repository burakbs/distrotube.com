---
title: "Linux Sucks For The New User"
image: images/thumbs/0155.jpg
date: Fri, 23 Mar 2018 22:03:35 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Linux+Sucks+For+The+New+User.mp4" >}}
&nbsp;

#### SHOW NOTES

Everyone is making "Linux Sucks" videos. I've had some requests to make a "Linux Sucks" video of my own. This is my take on the matter, and in regards to the new Linux user. And special thanks to Karl for the wallpapers:<a href="https://karl-schneider.deviantart.com/"> https://karl-schneider.deviantart.com/</a>    
