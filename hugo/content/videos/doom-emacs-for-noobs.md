---
title: "Doom Emacs For Noobs" 
image: images/thumbs/0717.jpg
date: 2020-09-19T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Doom+Emacs+For+Noobs.mp4" >}}
&nbsp;

#### SHOW NOTES

Doom Emacs is my preferred text editor, and I have made several videos about it.  But some of those videos assumed that the viewer had some knowledge of Vim and/or Emacs.  So I decided to make this Doom Emacs introductory video for the complete noob!  This video covers how to install Doom Emacs, how to configure it, and some of the basic keybindings and commands.   
 
REFERENCED:
+ https://github.com/hlissner/doom-emacs - Doom Emacs