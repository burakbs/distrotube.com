---
title: "All That Software On My PC. What Do I Actually Use?" 
image: images/thumbs/0726.jpg
date: 2020-10-02T12:23:40+06:00
author: Derek Taylor
tags: ["TUI Apps", "GUI Apps"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/all-that-software-on-my-pc-what-do-i/bdc668d67f3ccc5ad8549e22cfea266ff274538f?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

One of the most common questions I get on a recurring basis is "What software do you really use?"  Because I demonstrate so many programs on video, so many window managers, so many web browsers, so many terminal emulators...what programs do I actually use? 