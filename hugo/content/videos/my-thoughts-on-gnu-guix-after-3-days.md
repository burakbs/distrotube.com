---
title: "My Thoughts On GNU Guix After Three Days"
image: images/thumbs/0682.jpg
date: 2020-07-30T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Guix"]
---

#### VIDEO

{{< amazon src="My+Thoughts+On+GNU+Guix+After+Three+Days.mp4" >}}
&nbsp;

#### SHOW NOTES

I have spent several hours each of the last three days playing around with GNU Guix ( mostly watching packages build :D ).  I have it running in a VM and on a Lenovo Thinkpad.  There is a lot to love about Guix, but there are also some challenges with it.  Some of the problems I initially had were configuring Guix to recognize new window managers that I installed.  Also, running a "make install" doesn't work on my Suckless builds.  And qtile isn't packaged for Guix and a "pip install qtile" fails on Guix.  But I'm still having fun!

REFERENCED:
+ https://guix.gnu.org/