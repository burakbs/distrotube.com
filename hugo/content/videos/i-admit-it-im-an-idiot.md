---
title: "I Admit It. I'm An Idiot!"
image: images/thumbs/0652.jpg
date: 2020-06-22T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "Distro Reviews"]
---

#### VIDEO

{{< amazon src="I+Admit+It+Im+An+Idiot.mp4" >}}
&nbsp;

#### SHOW NOTES

A couple of weeks ago, I made a video about InstantOS.  It was a very negative video where I trashed InstantOS and InstantWM in some quite unfair ways.  So I would like to publicly apologize to the devs of InstantOS.

WANT AN ACTUAL GOOD REVIEW OF INSTANTOS?
Then watch this video from the InstantOS dev, Paper Benni.
+ https://www.youtube.com/watch?v=XTjAO3yUqpQ