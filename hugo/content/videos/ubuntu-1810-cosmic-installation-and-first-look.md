---
title: "Ubuntu 18.10 'Cosmic Cuttlefish' Installation and First Look"
image: images/thumbs/0298.jpg
date: Fri, 19 Oct 2018 23:58:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu"]
---

#### VIDEO

{{< amazon src="Ubuntu+1810+Cosmic+Cuttlefish+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Ubuntu 18.10, codenamed "Cosmic Cuttlefish", was released yesterday.  It
 features the GNOME 3.30 desktop, Firefox 63, LibreOffice 6, as well as 
other utilities.  

<a href="https://www.youtube.com/redirect?redir_token=MLG4fQTsqe4D9UG8Hv1A7O9btKF8MTU1MzQ3MTkwNkAxNTUzMzg1NTA2&amp;q=https%3A%2F%2Fwww.ubuntu.com%2F&amp;event=video_description&amp;v=LbxneTKg-SQ" rel="noreferrer noopener" target="_blank">https://www.ubuntu.com/
