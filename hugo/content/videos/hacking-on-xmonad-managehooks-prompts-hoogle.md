---
title: "Hacking On Xmonad - ManageHooks, Prompts and Hoogle"
image: images/thumbs/0627.jpg
date: 2020-05-20T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Hacking+On+Xmonad+-+ManageHooks%2C+Prompts+and+Hoogle.mp4" >}}
&nbsp;

#### SHOW NOTES


I discuss a few important things that you should know about if you thinking about switching to Xmonad.  ManageHooks sets window rules.  Prompts are  dmenu-like prompts that are builtin to Xmonad.  And Hoogle is the search engine that you need to use to search the Haskell documentation.

REFERENCED:
+ https://gitlab.com/dwt1/dotfiles/-/blob/master/.xmonad/xmonad.hs - My config
+ https://wiki.haskell.org/Xmonad/General_xmonad.hs_config_tips#ManageHook_examples - ManageHook Examples
+ https://hackage.haskell.org/package/xmonad-contrib-0.13/docs/XMonad-Prompt.html - Xmonad Prompt
+ https://hackage.haskell.org/package/base-4.14.0.0/docs/Data-Maybe.html - The "Maybe" type
+ https://hoogle.haskell.org/ - Hoogle