---
title: "Have I Ever Been Tempted By Proprietary Software?"
image: images/thumbs/0619.jpg
date: 2020-05-10T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", ""]
---

#### VIDEO

{{< amazon src="Have+I+Ever+Been+Tempted+By+Proprietary+Software.mp4" >}}
&nbsp;

#### SHOW NOTES

I walk around in my backyard and talk about the free and open source software that frustrates me enough to switch to the proprietary alternatives...almost.