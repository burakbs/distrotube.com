---
title: "Live Mar 25, 2018 - Playing with Calculate Linux XFCE"
image: images/thumbs/0158.jpg
date: Sun, 25 Mar 2018 22:06:55 +0000
author: Derek Taylor
tags: ["Live Stream", "Calculate Linux", "XFCE"]
---

#### VIDEO

{{< amazon src="Live+Mar+25%2C+2018+-+Playing+with+Calculate+Linux+XFCE.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this live stream, I'm going to be playing around with my freshly installed Calculate Linux XFCE. I may need some help with Calculate and with XFCE so if you know a bit about Calculate (or Gentoo) and XFCE, feel free to chime in with suggestions and tips. Should be fun! 
