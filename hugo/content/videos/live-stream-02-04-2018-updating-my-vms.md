---
title: "Live Stream Feb 4, 2018 - Updating all my VMs rather than watching SuperBowl"
image: images/thumbs/0107.jpg
date: Sun, 04 Feb 2018 01:43:13 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Live+Stream+Feb+4%2C+2018+-+Updating+all+my+VMs+rather+than+watching+SuperBowl.mp4" >}}  
&nbsp;

#### SHOW NOTES

I plan on launching each of the VMs that are currently on my computer and updating them. Some of them I haven't updated in two or three weeks so this will take some time. But while that is going on in the background, I will discuss some Linux-y stuff and answer any questions from viewers.
