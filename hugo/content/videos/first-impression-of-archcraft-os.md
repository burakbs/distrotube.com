---
title: "First Impression of Archcraft OS"
image: images/thumbs/0699.jpg
date: 2020-08-22T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Archcraft"]
---

#### VIDEO

{{< amazon src="First+Impression+of+Archcraft+OS.mp4" >}}
&nbsp;

#### SHOW NOTES

Archcraft OS is yet another minimal linux distribution, based on Arch Linux.  It's supposed to be light, fast, and attractive.  It only comes with window managers installed instead of full desktop environments, and it's supposed to be unixporn-worthy out of the box.  But is it all style and no function?

REFERENCED:
+ https://archcraft-os.github.io/