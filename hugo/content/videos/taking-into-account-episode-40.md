---
title: "Taking Into Account, Ep. 40 - New Red Hat, GNOME is a Mess, Librem One, Win 10 Bloated, Mozilla IRC"
image: images/thumbs/0387.jpg
date: Thu, 02 May 2019 03:04:44 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=48s">0:48 New red hat, same Red Hat.  The new logo for the Linux giant is unveiled.  What do you think? 

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=295s">4:55 The desktop in Ubuntu and Fedora. GNOME 3.32 is awesome, but still needs improvements in key areas. 

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=849s">14:09 Purism’s Librem One is a dream come true for privacy concerned people. 

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=1102s">18:22 Windows 10 will soon require at least 32GB of free space on your hard drive. 

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=1304s">21:44 Mozilla to drop IRC as main communications platform. Moving to Discord instead. 

<a href="https://www.youtube.com/watch?v=C6JHWTzasNo&amp;t=1664s">27:44 Many of you have been wondering how I like my new computer... 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=C6JHWTzasNo&amp;event=video_description&amp;redir_token=NdisGTtJV0vAl6Q9DFppF5kVMCF8MTU1NzE5ODI4NkAxNTU3MTExODg2&amp;q=https%3A%2F%2Fwww.redhat.com%2Fen%2Fabout%2Fbrand%2Fnew-brand" target="_blank">https://www.redhat.com/en/about/brand...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=C6JHWTzasNo&amp;event=video_description&amp;redir_token=NdisGTtJV0vAl6Q9DFppF5kVMCF8MTU1NzE5ODI4NkAxNTU3MTExODg2&amp;q=https%3A%2F%2Fjatan.tech%2F2019%2F05%2F01%2Fgnome-3-32-is-awesome-but-still-has-key-areas-for-improvements%2F" target="_blank">https://jatan.tech/2019/05/01/gnome-3...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=C6JHWTzasNo&amp;event=video_description&amp;redir_token=NdisGTtJV0vAl6Q9DFppF5kVMCF8MTU1NzE5ODI4NkAxNTU3MTExODg2&amp;q=https%3A%2F%2Fitsfoss.com%2Flibrem-one%2F" target="_blank">https://itsfoss.com/librem-one/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=C6JHWTzasNo&amp;event=video_description&amp;redir_token=NdisGTtJV0vAl6Q9DFppF5kVMCF8MTU1NzE5ODI4NkAxNTU3MTExODg2&amp;q=https%3A%2F%2Fwww.theinquirer.net%2Finquirer%2Fnews%2F3074878%2Fwindows-10-may-update-32gb-storage-requirement" target="_blank">https://www.theinquirer.net/inquirer/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=C6JHWTzasNo&amp;event=video_description&amp;redir_token=NdisGTtJV0vAl6Q9DFppF5kVMCF8MTU1NzE5ODI4NkAxNTU3MTExODg2&amp;q=https%3A%2F%2Fwww.ghacks.net%2F2019%2F04%2F28%2Fmozilla-to-drop-irc-as-main-communications-platform%2F" target="_blank">https://www.ghacks.net/2019/04/28/moz...
