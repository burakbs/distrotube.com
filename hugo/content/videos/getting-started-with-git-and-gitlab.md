---
title: "Getting Started With Git and GitLab"
image: images/thumbs/0698.jpg
date: 2020-08-21T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "git"]
---

#### VIDEO

{{< amazon src="Backup+Your+Configs+With+Git+and+GitLab.mp4" >}}
&nbsp;

#### SHOW NOTES

I use GitLab to host my git repositories.  Those repositories store my dotfiles, my scripts, my Suckless builds, my wallpapers, and much more.  Using git and a host like GitLab makes reinstalling your OS (aka distrohopping, to you Linux users) so much easier.