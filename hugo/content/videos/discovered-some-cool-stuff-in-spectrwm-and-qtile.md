---
title: "Discovered Some Cool Stuff In Spectrwm and Qtile"
image: images/thumbs/0597.jpg
date: 2020-04-15T12:23:40+06:00
author: Derek Taylor
tags: ["spectrwm", "qtile", "Tiling Window Managers"]
---

#### VIDEO

{{< amazon src="Discovered+Some+Cool+Stuff+In+Spectrwm+and+Qtile.mp4" >}}
&nbsp;

#### SHOW NOTES

So I found some cool things that you can do with Spectrwm and Qtile.  In Spectrwm, the man page has some good documentation on customizing the bar.  In Qtile, I read some of the python code and discovered a neat feature that I didn't know existed, and probably most Qtile users don't know about.

REFERENCED:
+ https://github.com/conformal/spectrwm
+ http://www.qtile.org/