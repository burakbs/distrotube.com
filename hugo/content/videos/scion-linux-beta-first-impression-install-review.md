---
title: "Scion Linux Beta First Impression Install & Review"
image: images/thumbs/0142.jpg
date: Tue, 13 Mar 2018 21:33:16 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Scion Linux"]
---

#### VIDEO

{{< amazon src="Scion+Linux+Beta+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this quick installation and overview, I take a look at an Ubuntu-based distro that uses the Openbox window manager. That distribution is called Scion Linux. It is currently in beta so expect some bugs. Thanks to Karl Schneider for suggesting this distro. <a href="https://sourceforge.net/projects/scionlinux/">https://sourceforge.net/projects/scionlinux/</a>
