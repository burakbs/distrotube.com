---
title: "Making My XMonad And Emacs Configs More Readable"
image: images/thumbs/0668.jpg
date: 2020-07-14T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "xmonad", "emacs"]
---

#### VIDEO

{{< amazon src="Making+My+XMonad+And+Emacs+Configs+More+Readable.mp4" >}}
&nbsp;

#### SHOW NOTES

I spent several hours this week cleaning up my configs for XMonad and Emacs.  Why?  Well, my XMonad config had grown to more 1,000 lines in length.  This made it a bit unwieldy and hard-to-read.  My Emacs config was also suffering from readability issues due to all the comments I was leaving in the config.  I found a solution to both of these problems.