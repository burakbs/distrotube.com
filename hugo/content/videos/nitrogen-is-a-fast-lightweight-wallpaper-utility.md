---
title: "Nitrogen Is A Fast And Lightweight Wallpaper Utility"
image: images/thumbs/0568.jpg
date: 2020-03-13T12:22:40+06:00
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Nitrogen+Is+A+Fast+And+Lightweight+Wallpaper+Utility.mp4" >}}
&nbsp;

#### SHOW NOTES

Nitrogen is a fast and lightweight wallpaper browser.  It makes setting your wallpaper simple, especially for you guys that run multiple monitors.  Nitrogen is perfect for those that run window managers rather than full desktop environments.

REFERENCED:
+ https://wiki.archlinux.org/index.php/Nitrogen