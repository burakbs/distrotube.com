---
title: "A Look at How I Setup Virtualbox For Testing Linux Distros"
image: images/thumbs/0068.jpg
date: Tue, 19 Dec 2017 15:27:40 +0000
author: Derek Taylor
tags: ["Virtualbox"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-look-at-how-i-setup-virtualbox-for/77e80f6c65c5fe6f2b940f8ba1e97cd98a705dc0?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I discuss how I use Virtualbox to install and test out Linux distros. I run through some of the settings I use and some of the features of Virtualbox. <a href="https://www.virtualbox.org/">https://www.virtualbox.org/</a>
