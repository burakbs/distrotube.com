---
title: "The Free Software Song - DT and RMS Duet"
image: images/thumbs/0403.jpg
date: Sun, 26 May 2019 17:36:14 +0000
author: Derek Taylor
tags: ["Richard Stallman", ""]
---

#### VIDEO

{{< amazon src="The+Free+Software+Song+DT+and+RMS+Duet.mp4" >}}
&nbsp;

#### SHOW NOTES

The original Stallman video link can be found at: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=9sJUDx7iEJw">https://www.youtube.com/watch?v=9sJUD... I am using the above video in accordance to the Copyright Disclaimer Under Section 107 of the Copyright Act 1976, where allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research.
