---
title: "Top Five Reasons To Run Arch Linux"
image: images/thumbs/0249.jpg
date: Mon, 23 Jul 2018 00:33:05 +0000
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Top+Five+Reasons+To+Run+Arch+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

I give you my top five reasons to run Arch Linux.
