---
title: "DT LIVE - Redcore Linux 1908 Plasma"
image: images/thumbs/0509.jpg
date: 2019-08-26T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Redcore Linux"]
---

#### VIDEO

{{< amazon src="DT+LIVE+-+Redcore+Linux+1908+Plasma-qCTLySCpVvI.mp4" >}}
&nbsp;

#### SHOW NOTES

On this livestream I plan to take a quick look at Redcore Linux 1908 Plasma.  Redcore is one of my favorite Gentoo-based distros, so I just have to take it for a spin.  Also, I'll be interacting with you guys hanging out in the YouTube chat.  :D

REFERENCED:
+ https://redcorelinux.org/
