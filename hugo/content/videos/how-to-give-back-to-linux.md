---
title: "How to Give Back to Linux and the Free and Open Source Communities"
image: images/thumbs/0192.jpg
date: Tue, 01 May 2018 22:53:22 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="How+to+Give+Back+to+Linux+and+the+Free+and+Open+Source+Communities.mp4" >}}
&nbsp;

#### SHOW NOTES

New Linux users often ask "how can I give back to Linux?" This is a quick video listing a variety of ways that people can contribute to the free and open source projects that they love, use and support! Referenced in this video: https://debian-handbook.info/browse/vi-VN/stable/sect.becoming-package-maintainer.html
