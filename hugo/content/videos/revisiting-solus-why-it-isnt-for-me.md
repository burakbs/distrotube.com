---
title: "Revisiting Solus. Why It Isn't Right For Me, Yet."
image: images/thumbs/0061.jpg
date: Fri, 15 Dec 2017 15:18:09 +0000
author: Derek Taylor
tags: ["Solus"]
---

#### VIDEO

{{< amazon src="Revisiting+Solus.+Why+It+Isnt+Right+For+Me%2C+Yet..mp4" >}} 
&nbsp;

#### SHOW NOTES

A quick video about my experience with Solus so far and why I will not be installing Solus on my main production machine.    
