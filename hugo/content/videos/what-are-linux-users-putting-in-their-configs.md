---
title: "What Are Linux Users Putting In Their Config Files?"
image: images/thumbs/0593.jpg
date: 2020-04-10T12:23:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "Vim"]
---

#### VIDEO

{{< amazon src="What+Are+Linux+Users+Putting+In+Their+Config+Files.mp4" >}}
&nbsp;

#### SHOW NOTES

Have you ever wondered what most Linux users are putting in their dotfiles?  One person decided to crawl GitHub for some of the common dotfiles most of us use (such as the bashrc, vimrc, xinitrc, etc)  He created a detailed report on the most popular settings within each file.  It's quite fascinating.

REFERENCED:
+ https://github.com/Kharacternyk/dotcommon - The Full Report