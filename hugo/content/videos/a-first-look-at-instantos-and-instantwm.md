---
title: "A First Look At InstantOS And InstantWM"
image: images/thumbs/0642.jpg
date: 2020-06-08T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "Distro Reviews"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-first-look-at-instantos-and-instantwm/acd5b136fb9bffe272b0f27280c56f4f5989fe40?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

InstantOS is an Arch-based Linux distribution with its own tiling window manager called InstantWM.  This is beta software so expect some bugs.  Here is a quick first look and first impression of InstantOS and InstantWM.

REFERENCED:
+ https://instantos.github.io/