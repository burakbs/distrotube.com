---
title: "Manjaro Openbox Install and Review"
image: images/thumbs/0202.jpg
date: Thu, 10 May 2018 23:07:47 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Manjaro", "Openbox"]
---

#### VIDEO

{{< amazon src="Manjaro+Openbox+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a quick look at Manjaro Openbox, a community edition of Manjaro that sports the light and fast Openbox window manager.
