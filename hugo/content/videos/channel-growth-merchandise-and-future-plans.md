---
title: "Channel Growth, Merchandise and Future Plans"
image: images/thumbs/0216.jpg
date: Thu, 24 May 2018 23:36:59 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Channel+Growth%2C+Merchandise+and+Future+Plans.mp4" >}}
&nbsp;

#### SHOW NOTES

Just a quick video marking a couple of milestones in the channel's growth (crossed 6,000 subscribers and 500,000 views this morning) and saying thank you to all of you have made this show successful. Also, I discuss some new merchandise offerings as well another option for making donations to the channel. Also discuss some potential future plans for the channel. Love you guys!
