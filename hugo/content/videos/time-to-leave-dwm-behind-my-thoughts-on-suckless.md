---
title: "Time To Leave Dwm Behind - My Thoughts on Suckless"
image: images/thumbs/0350.jpg
date: Mon, 11 Feb 2019 22:52:59 +0000
author: Derek Taylor
tags: ["tiling window managers", "dwm", "suckless"]
---

#### VIDEO

{{< amazon src="Time+To+Leave+Dwm+Behind+My+Thoughts+on+Suckless.mp4" >}}
&nbsp;

#### SHOW NOTES

I've used dwm for 23 days--two days longer than the 21 days I spent in  i3.  Like i3, three weeks was enough time for me to figure out most  everything about dwm, so it's time for me to move on to a new window  manager soon.   So I'm going to show you what my final dwm desktop looks  like and share my opinion on the suckless philosophy. 

📰 REFERENCED: <a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fsuckless.org%2F&amp;redir_token=X-s6dIm5joA_jcTjYhK2tNst-ll8MTU1MzY0MDgwNEAxNTUzNTU0NDA0&amp;v=wgRZNPxErEs&amp;event=video_description" target="_blank">https://suckless.org/
