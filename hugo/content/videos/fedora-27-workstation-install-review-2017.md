---
title: "Fedora 27 Workstation Installation and Review"
image: images/thumbs/0033.jpg
date: Tue, 14 Nov 2017 14:10:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Fedora", "GNOME"]
---

#### VIDEO

{{< amazon src="Fedora+27+Workstation+Installation+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

Today was the release date for Fedora 27. I install and review Fedora Workstation which features the Gnome 3.26 desktop environment. WANT TO SUPPORT THE CHANNEL? Please like, share and subscribe. And check out my Patreon page: <a href="https://www.patreon.com/distrotube">https://www.patreon.com/distrotube</a>     Your support is very much appreciated. Thanks, guys!
