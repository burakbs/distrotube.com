---
title: "Useful Tools Within Emacs For Writers"
image: images/thumbs/0691.jpg
date: 2020-08-11T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Useful+Tools+Within+Emacs+For+Writers.mp4" >}}
&nbsp;

#### SHOW NOTES

There seems to be a misconception about Emacs.  Many think that Emacs is only useful for programming.  Nothing could be further from the truth.  If you do any kind of writing, you owe it to yourself to take a look at the tools that Emacs offers.

REFERENCED:
+ https://orgmode.org/
+ https://www.gnu.org/software/emacs-muse/
+ https://www.emacswiki.org/emacs/wc-mo...
+ http://bnbeckwith.com/code/writegood-...
+ https://github.com/joostkremers/write...
