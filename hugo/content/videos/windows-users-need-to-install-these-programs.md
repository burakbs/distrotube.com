---
title: "Windows Users Need To Install These Programs IMMEDIATELY!" 
image: images/thumbs/0708.jpg
date: 2020-09-07T12:23:40+06:00
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Windows+Users+Need+To+Install+These+Programs.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you a Windows user?  If so, have you heard about free and open source software (or FOSS)?  Free and open source software is safer, more secure, and often just plain better than the proprietary, closed source software that comes pre-installed on Windows.  I will show you some of the best FOSS programs available on Windows.

REFERENCED:
+ https://www.mozilla.org/ - Firefox
+ https://www.thunderbird.net/ - Thunderbird
+ https://www.libreoffice.org/ - LibreOffice
+ https://www.videolan.org/ - VLC
+ https://www.gimp.org/ - GIMP
+ https://inkscape.org/ - Inkscape
+ https://www.audacityteam.org/ - Audacity
+ https://kdenlive.org/ - Kdenlive
+ https://obsproject.com/ - OBS
+ https://filezilla-project.org/ - FileZilla
+ https://www.7-zip.org/ - 7Zip