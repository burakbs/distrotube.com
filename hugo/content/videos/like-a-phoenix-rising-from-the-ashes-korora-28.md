---
title: "Like A Phoenix Rising From The Ashes - Korora 28 Xfce"
image: images/thumbs/0260.jpg
date: Sun, 12 Aug 2018 19:17:17 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Korora", "XFCE"]
---

#### VIDEO

{{< amazon src="Like+A+Phoenix+Rising+From+The+Ashes+-+Korora+28+Xfce.mp4" >}}
&nbsp;

#### SHOW NOTES

A few weeks ago, the main dev of Korora announced that the distro would 
no longer be developed.  In short, Korora was a dead project.   But, in 
true Linux fashion, the community rallied and decided to keep Korora 
going.

<a href="https://www.youtube.com/redirect?v=QHh-Ybwq3dM&amp;event=video_description&amp;q=https%3A%2F%2Fkororaproject.org%2Fabout%2Fnews%2Fthe-community-is-working-on-korora-28&amp;redir_token=JNGe452nUfFIA4cyWFDaBsZMT5B8MTU1MzQ1NTA0M0AxNTUzMzY4NjQz" rel="noreferrer noopener" target="_blank">https://kororaproject.org/about/news/...
