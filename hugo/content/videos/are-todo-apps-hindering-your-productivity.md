---
title: "Are TODO Applications Hindering Your Productivity?"
image: images/thumbs/0659.jpg
date: 2020-07-03T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/are-todo-applications-hindering-your/a2d82b21928f258900957b831d4d61ab694dd9b0?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I often showcase productivity applications on video.  One of the most popular kinds of productivity apps is the todo app.  Although, I've showcased some of these on video, I personally never use them.  The reason is that I don't consider todo apps an efficient or a productive use of my time.