---
title: "Virt-Manager Is The Better Way To Manage VMs"
image: images/thumbs/0577.jpg
date: 2020-03-24T12:22:40+06:00
author: Derek Taylor
tags: ["Virtualization", ""]
---

#### VIDEO

{{< amazon src="Shortwave+Internet+Radio+Player+For+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

KVM/QEMU/Virt-Manager is a hypervisor and a good alternative to Virtualbox.  Performance should be better using this combination since KVM is built into the Linux kernel itself.  And it's not difficult to convert your existing Virtualbox VM's over to Virt-Manager.

#### SOME COMMANDS I USED IN THIS VIDEO:
+ LC_ALL=C lscpu | grep Virtualization
+ yay -S qemu virt-manager ebtables
+ sudo systemctl enable libvirtd
+ sudo systemctl start libvirtd
+ sudo systemctl status libvirtd
+ sudo usermod -G libvirt -a dt
+ sudo qemu-img convert -f vdi -O qcow2 Ubuntu\ 20.04.vdi /var/lib/libvirt/images/ubuntu-20-04.qcow2

#### REFERENCED:
+ https://wiki.archlinux.org/index.php/KVM - KVM
+ https://wiki.archlinux.org/index.php/... - QEMU
+ https://wiki.archlinux.org/index.php/... - Libvirt
+ https://virt-manager.org/ - Virt-manager