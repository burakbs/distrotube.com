---
title: "Korora 26 First Impression Install & Review"
image: images/thumbs/0035.jpg
date: Fri, 17 Nov 2017 14:15:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Korora", "GNOME"]
---

#### VIDEO

{{< amazon src="Korora+26+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I'm reviewing a Fedora-based Linux distribution called Korora. This version of Korora is 26 (codenamed "Bloat") and sports a very nice-looking Gnome 3 desktop. For those users that are put off by Fedora's insistence of only shipping with free software and not being that user-friendly when it comes to installing third-party drivers and codecs, Korora may be the distro you are looking for! <a href="https://kororaproject.org/">https://kororaproject.org/</a>
