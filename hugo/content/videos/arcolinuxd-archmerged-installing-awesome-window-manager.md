---
title: "ArcoLinuxD (ArchMergeD) - Installing Awesome Window Manager"
image: images/thumbs/0137.jpg
date: Thu, 08 Mar 2018 21:27:10 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArcoLinux", "awesomewm"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/arcolinuxd-archmerged-installing-awesome/ead1f7b27fd8db876574696acad53db89a8fa546?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video, I go through the installation procedure for ArcoLinuxD (formerly ArchMergeD). Then I install their vision of the AwesomeWM on top of ArcoLinuxD. For those of you who love minimal window managers and love an Arch base, check out what ArcoLinux is doing. They are changing the game! <a href="https://arcolinux.info/">https://arcolinux.info/</a>
