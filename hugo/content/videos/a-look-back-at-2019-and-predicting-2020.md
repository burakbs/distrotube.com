---
title: "A Look Back At 2019 And Predicting 2020"
image: images/thumbs/0504.jpg
date: 2019-12-27T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-look-back-at-2019-and-predicting-2020/9104330cebc7da103b7b0924c0f958be59921e16?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Let's take a look back at some of the big stories for Linux in 2019.  Let's also think about what 2020 has in store for us.  I will make my bold predictions for next year.
