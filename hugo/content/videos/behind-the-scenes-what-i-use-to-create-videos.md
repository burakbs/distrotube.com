---
title: "Behind the Scenes Look - What do I use to create my videos?"
image: images/thumbs/0126.jpg
date: Mon, 26 Feb 2018 21:12:21 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Behind+the+Scenes+Look+-+What+do+I+use+to+create+my+videos.mp4" >}}  
&nbsp;

#### SHOW NOTES

I have gotten a lot of questions from viewers about the software I use to create the videos on this channel. I thought I'd do a quick video on the programs that work for me and my use case. Hopefully, you aspiring Youtubers can find something useful in this video. This video was requested by one of our regular viewers, Kerwin: <a href="https://www.youtube.com/channel/UCR8JcCXQqyazkNvRKK9secw">https://www.youtube.com/channel/UCR8JcCXQqyazkNvRKK9secw</a>
