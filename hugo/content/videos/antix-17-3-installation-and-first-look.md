---
title: "AntiX 17.3 Installation And First Look"
image: images/thumbs/0324.jpg
date: Sat, 29 Dec 2018 19:43:12 +0000
author: Derek Taylor
tags: ["Distro Reviews", "AntiX"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/antix-17-3-installation-and-first-look/14f7526a1990fea9f1ecbb1b11c7fdadebff7193?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm taking a look at the recently released AntiX 17.3.  AntiX is a
 fast and lightweight Linux distro based on Debian (Debian stable unless
 you're a mad man!).  AntiX comes with 4 window managers installed out 
of the box: fluxbox, icewm, jwm and herbstluftwm.

<a href="https://www.youtube.com/redirect?v=v8WC07Sumhs&amp;event=video_description&amp;redir_token=uyJ04HmMfF_wbMXI3z_jy1tiBk98MTU1MzU0MzAwM0AxNTUzNDU2NjAz&amp;q=https%3A%2F%2Fantixlinux.com%2F" rel="noreferrer noopener" target="_blank">https://antixlinux.com/
