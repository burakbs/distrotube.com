---
title: "Is Vivaldi Open Source Software? Myth Debunked!"
image: images/thumbs/0613.jpg
date: 2020-05-03T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Is+Vivaldi+Open+Source+Software+Myth+Debunked.mp4" >}}
&nbsp;

#### SHOW NOTES

So there is a disturbing trend in my YouTube comments.  I have people telling me that I am wrong labeling the Vivaldi web browser as "proprietary garbage".  They claim it is, in fact, "open source".  Where do these people get this misinformation?

REFERENCED:
+ https://help.vivaldi.com/article/is-vivaldi-open-source/
+ https://vivaldi.com/privacy/vivaldi-end-user-license-agreement/