---
title: "Day 1 - Terminal Color Scheme and Mutt Email"
image: images/thumbs/0174.jpg
date: Wed, 11 Apr 2018 22:26:50 +0000
author: Derek Taylor
tags: ["terminal", "email"]
---

#### VIDEO

{{< amazon src="Day+1+-+Terminal+Color+Scheme+and+Mutt+Email.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 1 of my 30 challenge. I'm living in a tiling window manager for the next 30 days and only using terminal-based applications where possible. Setting up the Solarized color scheme in the Termite terminal emulator. Setting up Neomutt email client.
