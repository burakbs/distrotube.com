---
title: "Chat With Patrons (January 6, 2019)"
image: images/thumbs/0483.jpg
date: Sun, 06 Jan 2019 19:02:44 +0000
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(January+6%2C+2019)-IPT39dyee0s.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be a special event for my Patreon supporters. This will be a Zoom video chat just for my patrons. The link to the video chat call will be posted on my Patreon page a few minutes prior to the stream.
