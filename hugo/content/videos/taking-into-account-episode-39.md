---
title: "Taking Into Account, Ep. 39 - Let's Do It LIVE!"
image: images/thumbs/0402.jpg
date: Thu, 25 Apr 2019 14:52:58 +0000
author: Derek Taylor
tags: ["Taking Into Account", "Live Stream"]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+39+-+Let's+Do+It+LIVE!-ZkMYoSQIFw0.mp4" >}}
&nbsp;

#### SHOW NOTES

On this LIVE edition of Taking Into Account...


&nbsp;
#### REFERENCED:
+ https://betanews.com/2019/04/24/scientific-linux-dead/
+ https://www.wraltechwire.com/2019/04/25/red-hats-signature-fedora-logo-to-disappear-from-hq-tower-may-1/
+ https://blog.mozilla.org/blog/2019/04/23/its-complicated-mozillas-2019-internet-health-report/
+ https://arstechnica.com/gadgets/2019/04/microsoft-3q19-revenue-up-14-on-the-back-of-strong-cloud-and-uh-windows/https://fossbytes.com/ubuntu-19-10-features-release-date-download-codename/
