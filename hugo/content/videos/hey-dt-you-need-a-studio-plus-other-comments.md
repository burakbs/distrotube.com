---
title: "'Hey, DT. You Need A Better Studio!' (Plus Other Comments I Get)" 
image: images/thumbs/0716.jpg
date: 2020-09-18T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

{{< amazon src="Hey+DT+You+Need+A+Proper+Studio+Plus+Other+Viewer+Comments.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions and comments that I've been receiving from viewers.  I discuss alternatives to the Ubuntu Software Center, alternatives to the term "proprietary garbage", what software you should install alongside your window managers in Arch Linux, VirtualBox versus Virt-Manager, and my recording setup and why I need a proper studio.