---
title: "Bodhi Linux 5.0.0 Released - Let's Take A Look"
image: images/thumbs/0265.jpg
date: Wed, 22 Aug 2018 22:42:21 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Bodhi Linux"]
---

#### VIDEO

{{< amazon src="Bodhi+Linux+5.0.0+Released+-+Lets+Take+A+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Bodhi Linux 5.0.0 was released yesterday. I take it for a quick spin in a VM. Bodhi is based on Ubuntu 18.04 and sports the Moksha desktop environment. It's very light and blazing fast! <a href="https://www.bodhilinux.com/">https://www.bodhilinux.com/
