---
title: "GNOME Promotes Racism And Should Be Cancelled"
image: images/thumbs/0643.jpg
date: 2020-06-10T12:23:40+06:00
author: Derek Taylor
tags: ["GNOME", "FOSS Advocacy"]
---

#### VIDEO

{{< amazon src="GNOME+Promotes+Racism+And+Should+Be+Cancelled.mp4" >}}
&nbsp;

#### SHOW NOTES

GNOME recently posted an announcement that they support minorities and stand firm with Black Lives Matter.  They claim to support everyone "no matter what the color of your skin, your accent, or your background." This caused a bit of a stink, even within the GNOME community, because GNOME has a Code of Conduct that has some rather divisive and discriminatory language.

REFERENCED:
+ https://wiki.gnome.org/Foundation/CodeOfConduct