---
title: "Completing Our Bash Script - More With Variables, Arrays And If-Then Statements"
image: images/thumbs/0461.jpg
date: Fri, 08 Nov 2019 00:14:00 +0000
author: Derek Taylor
tags: ["shell", "shell scripting", "command line"]
---

#### VIDEO

{{< amazon src="Completing+Our+Bash+Script+More+With+Variables+Arrays+And+If+Then.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to continue to work on our bash script from the video I did a couple of weeks ago.

#### REFERENCED: 
+ The first video: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/watch?v=xhI1qXUrAHw">https://www.youtube.com/watch?v=xhI1q...
+ The completed script: <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles%2Fblob%2Fmaster%2Fnew.sh&amp;v=f3KwYamM4_M&amp;redir_token=uCAsUohV3jyGt-Fe_p_RAzqhuPd8MTU3NzQ5MjA4NkAxNTc3NDA1Njg2&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://gitlab.com/dwt1/dotfiles/blob...
