---
title: "A Linuxy Live Stream"
image: images/thumbs/0503.jpg
date: 2019-11-13T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-linuxy-live-stream/c2bdb7593d28c05d4fd8483b7c2d8716a5c87a09?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

This Linuxy live stream will just be me hanging out with you.  I might do some Linux stuff.  I might talk about some Linux stuff.  I might just do a Q&A with you guys hanging out in the chat.  This stream might be fun and entertaining, or it could be a complete train wreck.  With my streams, you never know what you're gonna get.
