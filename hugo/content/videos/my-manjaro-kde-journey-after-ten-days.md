---
title: "My Manjaro KDE Journey, After Ten Days"
image: images/thumbs/0082.jpg
date: Sat, 06 Jan 2018 01:08:26 +0000
author: Derek Taylor
tags: ["Manjaro", "KDE"]
---

#### VIDEO

{{< amazon src="My+Manjaro+KDE+Journey%2C+After+Ten+Days.mp4" >}}  
&nbsp;

#### SHOW NOTES

Keeping you guys up-to-date on my experience living in Manjaro KDE on my main machine. So far I'm loving Manjaro as a distribution. And so far, I'm not that in love with KDE as a desktop environment. In the video, I explain some of my grievances toward KDE.

---
