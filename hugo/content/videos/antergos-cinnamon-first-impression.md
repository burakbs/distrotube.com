---
title: "Antergos Cinnamon First Impression Install & Review"
image: images/thumbs/0006.jpg
date: Tue, 10 Oct 2017 01:44:27 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Antergos", "Cinnamon"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/antergos-cinnamon-first-impression/6d9a51444612e9b257e580891cf1cead70619068?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today I'm installing Antergos Linux for the first time. Going to run through the install and give my first impression on the installer and the Antergos Cinnamon edition. I will keep this installation updated and will revisit in the coming weeks and months ahead to see how things progress.
