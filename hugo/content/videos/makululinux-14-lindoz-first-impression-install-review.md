---
title: "MakuluLinux 14 LinDoz First Impression Install & Review"
image: images/thumbs/0089.jpg
date: Mon, 18 Jan 2018 01:16:54 +0000
author: Derek Taylor
tags: ["Distro Reviews", "MakuluLinux"]
---

#### VIDEO

{{< amazon src="MakuluLinux+14+LinDoz+First+Impression+Install+%26+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this first impression install and review video, I take a quick look at MakuluLinux LinDoz--a distro based on Debian Testing that features a customized Cinnamon desktop designed to mimic the Windows desktop.

http://www.makululinux.com/wp/lindoz/   
