---
title: "How To Secure The Firefox Browser"
image: images/thumbs/0554.jpg
date: 2020-02-25T12:22:40+06:00
author: Derek Taylor
tags: ["Privacy", "Security", "Firefox"]
---

#### VIDEO

{{< amazon src="How+To+Secure+The+Firefox+Browser.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the questions I often get is "How do I make my web browser more secure?"  In this video, I will show you a few things that you can do to make Mozilla Firefox more secure and more private.   

REFERENCED:
+ https://www.distrotube.com/blog/securing-the-firefox-web-browser/