---
title: "Etcher USB/SD Image Writer"
image: images/thumbs/0161.jpg
date: Wed, 28 Mar 2018 22:11:02 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Etcher+USBSD+Image+Writer.mp4" >}}  
&nbsp;

#### SHOW NOTES

I just wanted to share with you guys this simple-to-use USB/SD image writer program called Etcher. It is an electron program and makes writing bootable USB sticks as easy as inserting the device and clicking two buttons. <a href="https://etcher.io/">https://etcher.io/</a>
