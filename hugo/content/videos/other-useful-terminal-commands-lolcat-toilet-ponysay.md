---
title: "Other Useful Terminal Commands - lolcat, toilet, ponysay and more!"
image: images/thumbs/0120.jpg
date: Wed, 21 Feb 2018 21:01:06 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Other+Useful+Terminal+Commands+-+lolcat%2C+toilet%2C+ponysay+and+more!.mp4" >}}  
&nbsp;

#### SHOW NOTES

A few more very important terminal commands for the power user!
