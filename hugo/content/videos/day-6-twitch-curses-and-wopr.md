---
title: "Day 6 - Twitch Curses and WOPR"
image: images/thumbs/0179.jpg
date: Mon, 16 Apr 2018 22:34:45 +0000
author: Derek Taylor
tags: ["terminal"]
---

#### VIDEO

{{< amazon src="Day+6+-+Twitch+Curses+and+WOPR.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 6 of my 30 challenge. I'm living in a tiling window manager for the next 30 days and only using terminal-based applications where possible. I asked you guys for CLI app suggestions and you guys really come through. I share three terminal-based programs that I found interesting. 
