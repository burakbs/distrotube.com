---
title: "Virtual Machine Updates and Late Night Chat - DT LIVE"
image: images/thumbs/0215.jpg
date: Wed, 23 May 2018 23:35:43 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Virtual+Machine+Updates+and+Late+Night+Chat+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Just updating all of my VMs.
