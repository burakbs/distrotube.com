---
title: "Taking Into Account Ep. 5 - Valve, Gimp, GNOME, Summer Camp, App Store Tax"
image: images/thumbs/0266.jpg
date: Thu, 23 Aug 2018 22:44:21 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account.+Ep.+5+-+Valve%2C+Gimp%2C+GNOME%2C+Summer+Camp%2C+App+Store+Tax.mp4" >}}
&nbsp;

#### SHOW NOTES

This weeks topics include: 

+ 0:40 Valve makes Windows games run on Linux 
+ 6:48 Gimp released 2.10.16 with new features 
+ 12:05 GNOME extension puts files and folders back on desktop 
+ 17:05 Summer camp teaches kids about Linux 
+ 21:40 Revolt brewing over app store tax by Apple and Google 
+ 28:20 I read a viewer question 
