---
title: "Unfettered Freedom, Ep. 6 - Edge Isn't FOSS, Ubuntu Alternative, Patent Trolls, Clem, FSF Awards" 
image: images/thumbs/0710.jpg
date: 2020-09-09T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+6.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 2:17 - MS Edge is proprietary garbage. So is Vivaldi, and so is Chrome.
+ 11:43 - A viewer is looking for a "more free" distro than Ubuntu.
+ 19:11 - Lessons from a patent troll incident.
+ 25:13 - Clem Lefebvre, creator of Linux Mint, sits down for rare interview.
+ 33:40 - Linux Foundation fiasco, FSFE and the FSF, and FSF Award Nominations
+ 42:37 - Outro and a THANK YOU to the patrons!