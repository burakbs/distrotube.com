---
title: "Why Isn't Emacs More Popular?" 
image: images/thumbs/0724.jpg
date: 2020-09-28T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/why-isn-t-emacs-more-popular/259032ed5cffb458d413fef6dd841609ccd8b81a?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

The Emacs community has been debating whether it is time to make some changes with the software to make it more appealing to new users.  After all, Emacs is such an extremely powerful and unique piece of software, it deserves to have far more users than what it currently has.