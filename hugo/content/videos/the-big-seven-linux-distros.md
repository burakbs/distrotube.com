---
title: "The Big Seven Linux Distributions"
image: images/thumbs/0127.jpg
date: Tue, 27 Feb 2018 21:13:57 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Big+Seven+Linux+Distributions.mp4" >}}  
&nbsp;

#### SHOW NOTES

A brief discussion about the main Linux distributions that almost all other distros are based upon. I call these seven distros "the big seven." I also talk about the intended audience of these distros. In particular, I talk about the ones that are most appropriate for the new Linux user.
