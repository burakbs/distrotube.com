---
title: "A Quick Look At Slackel 7.3 Openbox"
image: images/thumbs/0667.jpg
date: 2020-07-13T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Slackel"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-quick-look-at-slackel-7-3-openbox/638228594b05cee2a8b1842e435235e1a410775d?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Slackel is a Linux distribution based on Slackware and Salix. It is fully compatible with Slackware but the difference is that it is based on the development branch.  Slackel comes in three editions, KDE, Openbox and MATE.  Today, I'm taking a look at the recently released Slackel 7.3 Openbox.

REFERENCED:
+ http://www.slackel.gr/forum/viewtopic.php?f=3&t=612