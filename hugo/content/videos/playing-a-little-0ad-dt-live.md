---
title: "Playing a Little 0 A.D. - DT LIVE"
image: images/thumbs/0208.jpg
date: Thu, 17 May 2018 23:22:42 +0000
author: Derek Taylor
tags: ["Gaming", "Live Stream"]
---

#### VIDEO

{{< amazon src="Playing+a+Little+0+A.D.+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Today I'm going to check out a really awesome realtime strategy game called 0 A.D. It is an Age of Empires type game that is free and open source software. Great gameplay and some fantastic graphics. https://play0ad.com/
