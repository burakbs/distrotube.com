---
title: "Play Music In Your Terminal With Music On Console"
image: images/thumbs/0594.jpg
date: 2020-04-11T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "CLI Apps"]
---

#### VIDEO

{{< amazon src="Play+Music+In+Your+Terminal+With+Music+On+Console.mp4" >}}
&nbsp;

#### SHOW NOTES

Music On Console (moc) is a minimal, lightweight music player.  It is a terminal-based application but don't let that scare you.  It's very easy to install, configure and use.  And if you want a nice way to control moc from your systray, then install mocicon as well.  

REFERENCED:
+ https://wiki.archlinux.org/index.php/MOC
+ https://github.com/mutantturkey/mocicon