---
title: "Lynx - Text-based Web Browser - Surf the Web From a Terminal"
image: images/thumbs/0024.jpg
date: Sat, 28 Oct 2017 02:27:42 +0000
author: Derek Taylor
tags: ["TUI Apps", "lynx", "Web Browsers"]
---

#### VIDEO

{{< amazon src="Lynx+-+Text-based+Web+Browser+-+Surf+the+Web+From+a+Terminal.mp4" >}}  
&nbsp;

#### SHOW NOTES

I review a text-based web browser called Lynx. It is a web browser that you run inside a terminal and is all text -- no images, no videos, no advertisements. Fast and lightweight. Highly configurable. http://lynx.browser.org/
