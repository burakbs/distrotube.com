---
title: "Rode PSA1 Scissor Arm and VocalBeat Windscreen for Blue Yeti"
image: images/thumbs/0146.jpg
date: Fri, 16 Mar 2018 21:39:12 +0000
author: Derek Taylor
tags: ["Hardware", ""]
---

#### VIDEO

{{< amazon src="Rode+PSA1+Scissor+Arm+and+VocalBeat+Windscreen+for+Blue+Yeti.mp4" >}}  
&nbsp;

#### SHOW NOTES

A short video of two new pieces of equipment I purchased.  
