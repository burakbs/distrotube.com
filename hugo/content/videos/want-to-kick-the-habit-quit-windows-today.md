---
title: "Want To Kick The Habit? Quit Windows Today!"
image: images/thumbs/0316.jpg
date: Sat, 08 Dec 2018 19:29:49 +0000
author: Derek Taylor
tags: ["Public Service Announcements", ""]
---

#### VIDEO

{{< amazon src="Want+To+Kick+The+Habit+Quit+WindowsToday!.mp4" >}}
&nbsp;

#### SHOW NOTES

Want to kick that Windows habit?  Do it today!

MUSIC:
"Kiss The Sky" by Aakash Gandhi ( <a href="https://www.youtube.com/user/88keystoeuphoria/videos">https://www.youtube.com/user/88keysto... )
The track can be found in the YouTube Audio Library.
