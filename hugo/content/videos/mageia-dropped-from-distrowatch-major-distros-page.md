---
title: "Mageia Dropped From DistroWatch 'Major Distros' Page. What Should Replace It?"
image: images/thumbs/0342.jpg
date: Mon, 28 Jan 2019 22:40:24 +0000
author: Derek Taylor
tags: ["Mageia", "DistroWatch"]
---

#### VIDEO

{{< amazon src="Mageia+Dropped+From+DistroWatch+Major+Distros+Page+What+Should+Replace+It.mp4" >}}
&nbsp;

#### SHOW NOTES

DistroWatch is dropping Mageia from it's "Major Distros" page, a listing  of what DistroWatch deems the ten most important Linux distros.   What  should replace Mageia on the list? 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdistrowatch.com%2F&amp;redir_token=lWqGzb77hXKlR1Gp8w_eceaF1h18MTU1MzY0MDA0NkAxNTUzNTUzNjQ2&amp;v=Aum1JguOgfE&amp;event=video_description" target="_blank">https://distrowatch.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdistrowatch.com%2Fdwres.php%3Fresource%3Dmajor&amp;redir_token=lWqGzb77hXKlR1Gp8w_eceaF1h18MTU1MzY0MDA0NkAxNTUzNTUzNjQ2&amp;v=Aum1JguOgfE&amp;event=video_description" target="_blank">https://distrowatch.com/dwres.php?res...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdistrowatch.com%2Fweekly.php%3Fissue%3D20190128%23poll&amp;redir_token=lWqGzb77hXKlR1Gp8w_eceaF1h18MTU1MzY0MDA0NkAxNTUzNTUzNjQ2&amp;v=Aum1JguOgfE&amp;event=video_description" target="_blank">https://distrowatch.com/weekly.php?is...
