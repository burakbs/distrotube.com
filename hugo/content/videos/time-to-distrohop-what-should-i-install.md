---
title: "Time to distrohop! What should I install?"
image: images/thumbs/0139.jpg
date: Sun, 11 Mar 2018 21:29:23 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Time+to+distrohop!+What+should+I+install.mp4" >}}  
&nbsp;

#### SHOW NOTES

I'm ready to change distros on my main production machine and my laptop. On the laptop, I think I've decided on Solus Budgie. For my main production machine, I'm not quite sure what I want to go with. I discuss some distros I'm considering, but I would love your suggestions. 
