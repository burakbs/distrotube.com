---
title: "Installing FreeBSD Is Quick And Easy"
image: images/thumbs/0648.jpg
date: 2020-06-18T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "FreeBSD"]
---

#### VIDEO

{{< amazon src="Installing+FreeBSD+Is+Quick+And+Easy.mp4" >}}
&nbsp;

#### SHOW NOTES

You guys have been wanting me to do a video on that "other" Unix-like operating system.  So I am going to do a quick installation of the recently released FreeBSD 11.4 inside a virtual machine.  FreeBSD is not difficult to install.  It's actually so simple to install, even a Boomer could do it!

REFERENCED:
+ https://www.freebsd.org/  
