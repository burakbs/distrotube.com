---
title: "Resurrecting an Old Laptop with Linux"
image: images/thumbs/0230.jpg
date: Fri, 15 Jun 2018 00:04:53 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Resurrecting+an+Old+Laptop+with+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

I have a family member's laptop that is struggling to function with Windows 7. I was given permission to put Linux on this machine in an attempt to breathe life back into it. 
NOTE: I mispoke when I said it was a dual core machine. Htop clearly shows four cores. The processor in this machine first shipped in early 2011. So this machine is probably around 7 years old.
