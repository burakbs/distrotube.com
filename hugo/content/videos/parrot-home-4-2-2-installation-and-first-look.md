---
title: "Parrot Home 4.2.2 Installation and First Look"
image: images/thumbs/0276.jpg
date: Sat, 15 Sep 2018 18:50:30 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Parrot"]
---

#### VIDEO

{{< amazon src="Parrot+Home+4.2.2+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Parrot Home 4.2.2.  It is a 
Debian-based Linux distro that features the MATE desktop environment and
 offers a full suite of desktop applications.

<a href="https://www.youtube.com/redirect?event=video_description&amp;v=k646rBvD5Ms&amp;q=http%3A%2F%2Fparrotsec.org%2F&amp;redir_token=XAklRs8xiAmLeR5jLrxUOb4Jjox8MTU1MzQ1MzQ1MEAxNTUzMzY3MDUw" rel="noreferrer noopener" target="_blank">http://parrotsec.org/
