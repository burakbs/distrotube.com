---
title: "Dtrx - An Easier Way To Extract Compressed Files"
image: images/thumbs/0268.jpg
date: Sun, 26 Aug 2018 18:12:37 +0000
author: Derek Taylor
tags: ["command line", ""]
---

#### VIDEO

{{< amazon src="Dtrx+-+An+Easier+Way+To+Extract+Compressed+Files.mp4" >}}
&nbsp;

#### SHOW NOTES

Dtrx stands for “Do The Right Extraction.” It's a tool for Unix-like 
systems that takes all the hassle out of extracting archives. 

<a href="https://www.youtube.com/redirect?redir_token=jYgGoZEBxbov9V6OyeawwSnYjPJ8MTU1MzQ1MTIzNUAxNTUzMzY0ODM1&amp;q=https%3A%2F%2Fgithub.com%2Fmoonpyk%2Fdtrx&amp;v=rxdsYyuBGG4&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://github.com/moonpyk/dtrx
