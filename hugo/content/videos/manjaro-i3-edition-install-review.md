---
title: "Manjaro i3 Edition Install & Review"
image: images/thumbs/0094.jpg
date: Sun, 21 Jan 2018 01:20:24 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Manjaro", "i3"]
---

#### VIDEO

{{< amazon src="Manjaro+i3+Edition+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a quick look at the Manjaro i3 edition which is a community release of Manjaro that features the tiling window manager "i3". Admittedly, this distro was a challenge for me to review because (1) it's a tiling window manager, and (2) I am not familiar with i3 at all. https://manjaro.org/community-editions/
