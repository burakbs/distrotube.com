---
title: "How Did You React To The Code Of Conduct News? Proud Of Your Actions?"
image: images/thumbs/0284.jpg
date: Tue, 18 Sep 2018 19:28:22 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="How+Did+You+React+To+The+Code+Of+Conduct+News+Proud+Of+Your+Actions.mp4" >}}
&nbsp;

#### SHOW NOTES

The last couple of days has seen a lot of fighting within the free and open source communities, especially regarding the news of the new Code of Conduct.  Were you one of the ones that participated in some of these heated debates on the Internet?  If so, are you proud of the way you behaved?
