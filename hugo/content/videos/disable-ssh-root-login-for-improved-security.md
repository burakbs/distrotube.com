---
title: "Disable SSH Root Login For Improved Security"
image: images/thumbs/0540.jpg
date: 2020-02-10T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "Security"]
---

#### VIDEO

{{< amazon src="Disable+SSH+Root+Login+For+Improved+Security.mp4" >}}
&nbsp;

#### SHOW NOTES

This is a just a quick video on how to disable root login via SSH.  Why disable the ability for to login via root?  Because most attackers are going to try to get in as root.

REFERENCED:
+  https://wiki.archlinux.org/index.php/OpenSSH#Limit_root_login