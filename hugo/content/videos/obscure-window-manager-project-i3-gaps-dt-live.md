---
title: "Obscure Window Manager Project - i3 Gaps - DT LIVE"
image: images/thumbs/0235.jpg
date: Sun, 24 Jun 2018 00:12:35 +0000
author: Derek Taylor
tags: ["Live Stream", "i3wm"]
---

#### VIDEO

{{< amazon src="Obscure+WIndow+Manager+Project+-+i3+Gaps+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

In this edition of the Obscure Window Manager Project, I take a look at i3 gaps. It is a manual tiling window manager with some unique features. I've taken a quick look at i3 a couple of times on the channel but have never gone in-depth with it, so this should be interesting.
