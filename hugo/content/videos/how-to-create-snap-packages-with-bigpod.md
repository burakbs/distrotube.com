---
title: "How To Create A Snap Package (with bigpod)"
image: images/thumbs/0633.jpg
date: 2020-05-28T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="How+To+Create+A+Snap+Package+(with+bigpod).mp4" >}}
&nbsp;

#### SHOW NOTES

Snap packages are a distro-agnostic packaging format for Linux.  In this collaborative video with fellow YouTuber and Linux community member, Bigpod, we go over the basic of creating snaps. 

REFERENCED:
+ https://www.youtube.com/user/bigpodgurc - Bigpod on YouTube
+ https://lbry.tv/@bigpod:1 - Bigpod on LBRY
+ https://snapcraft.io/