---
title: "Antergos GNOME Install & Review"
image: images/thumbs/0116.jpg
date: Sun, 18 Feb 2018 01:56:02 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Antergos", "GNOME"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/antergos-gnome-install-review/7b2e09046091e4e40d12717d212d60d0f3e75495?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video, I check out the GNOME edition of Antergos--a popular Arch-based Linux distribution that has a substantial following.
