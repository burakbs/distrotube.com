---
title: "Debian vs Arch. Which Is The Best Distro?"
image: images/thumbs/0419.jpg
date: Tue, 09 Jul 2019 05:37:00 +0000
author: Derek Taylor
tags: ["Debian", "Arch Linux"]
---

#### VIDEO

{{< amazon src="Debian+Versus+Arch+Which+Is+The+Best+Distro.mp4" >}}
&nbsp;

#### SHOW NOTES

Debian and Arch. Two of the most popular Linux distributions. Which is the better distro?

REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=yNUeqi3Ebv0&amp;event=video_description&amp;redir_token=vPgUsBwat5Dw7sbWTc_RFGCsBm98MTU3NTY5NzA4M0AxNTc1NjEwNjgz&amp;q=https%3A%2F%2Fwww.debian.org%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.debian.org/
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=yNUeqi3Ebv0&amp;event=video_description&amp;redir_token=vPgUsBwat5Dw7sbWTc_RFGCsBm98MTU3NTY5NzA4M0AxNTc1NjEwNjgz&amp;q=https%3A%2F%2Fwww.archlinux.org%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.archlinux.org/
