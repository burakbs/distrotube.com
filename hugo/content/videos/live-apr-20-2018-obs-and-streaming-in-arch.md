---
title: "Live Apr 20, 2018 - OBS streaming problems in Arch"
image: images/thumbs/0182.jpg
date: Fri, 20 Apr 2018 22:42:03 +0000
author: Derek Taylor
tags: ["Live Stream", "Arch Linux"]
---

#### VIDEO

{{< amazon src="Live+Apr+20%2C+2018+-+OBS+streaming+problems+in+Arch.mp4" >}}
&nbsp;

#### SHOW NOTES

A test stream of me playing with OBS...starting and stopping the stream...all while reading chat comments and having a good time. :D
