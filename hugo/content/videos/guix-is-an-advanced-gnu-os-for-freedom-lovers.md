---
title: "Guix Is An Advanced GNU Operating System For Freedom Lovers"
image: images/thumbs/0680.jpg
date: 2020-07-27T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Guix"]
---

#### VIDEO

{{< amazon src="Guix+Is+An+Advanced+GNU+Operating+System+For+Freedom+Lovers.mp4" >}}
&nbsp;

#### SHOW NOTES

Guix is an advanced distribution of the GNU operating system developed by the GNU Project. It is available as a GNU/Linux-libre distro or you can use Guix with GNU's HURD kernel.  Guix supports transactional upgrades, roll-backs, and unprivileged package management.  Guix is a 100% free distro and is approved by the Free Software Foundation.

REFERENCED:
 https://guix.gnu.org/