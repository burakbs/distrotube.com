---
title: "What's The Best 'Linux'? It's GNU/Linux!"
image: images/thumbs/0663.jpg
date: 2020-07-08T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="What's+The+Best+Linux+GNU+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the most often asked questions is what is the best "Linux?"  Well, the answer to this question is obvious.  But first, I'd just like to interject for a moment...

REFERENCED:
+ https://www.gnu.org/
+ Music is: Wishful Thinking by Dan Lebowitz, from the YouTube Audio Library