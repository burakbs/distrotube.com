---
title: "Ubuntu Studio 17.10 Artful Aardvark - Install and Review"
image: images/thumbs/0019.jpg
date: Sat, 21 Oct 2017 02:05:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu Studio", "XFCE"]
---

#### VIDEO

{{< amazon src="Ubuntu+Studio+17.10+Artful+Aardvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Let's take a look at Ubuntu Studio 17.10 -- the flavor of Ubuntu for those into audio, graphics, video, photography and publishing work. Ubuntu Studio 17.10 sports the lightweight XFCE desktop environment and comes preloaded with a ton of software. Ubuntu Studio - https://ubuntustudio.org/
