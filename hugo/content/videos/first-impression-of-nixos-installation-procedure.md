---
title: "First Impression of the NixOS Installation Procedure"
image: images/thumbs/0160.jpg
date: Tue, 27 Mar 2018 22:09:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "NixOS"]
---

#### VIDEO

{{< amazon src="First+Impression+of+the+NixOS+Installation+Procedure.mp4" >}}  
&nbsp;

#### SHOW NOTES

Been getting a lot of requests to take a look at NixOS. I don't really know anything about NixOS so I'm going to run through the install and see what Nix is all about. <a href="https://nixos.org/">https://nixos.org/</a>
