---
title: "Shortwave Internet Radio Player For Linux"
image: images/thumbs/0576.jpg
date: 2020-03-23T12:22:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "Linux Audio"]
---

#### VIDEO

{{< amazon src="Shortwave+Internet+Radio+Player+For+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

Shortwave is an Internet radio player for your Linux desktop.  It is a GTK application and is available as a Flatpak.  You can listen to more than 25,000 radio stations, and Shortwave automatically records the songs that you play!

REFERENCED:
+ https://gitlab.gnome.org/World/Shortwave