---
title: "Boost Productivity With Emacs, Org Mode and Org Agenda" 
image: images/thumbs/0722.jpg
date: 2020-09-25T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Boost+Productivity+With+Emacs%2C+Org+Mode+and+Org+Agenda.mp4" >}}
&nbsp;

#### SHOW NOTES

Do you use "productivity apps"?  If so, Emacs, Org Mode and Org Agenda lets you make todo lists, schedule tasks, manage projects and much more.  I've never been a "todo list" or "appointment scheduling" kind of person but the more I play with Emacs and Org, the more I think that I should be doing these things.

REFERENCED:
+  https://github.com/hlissner/doom-emacs - Doom Emacs
+  https://orgmode.org - Org Mode