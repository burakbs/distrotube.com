---
title: "Social Media Causes Mental Illness"
image: images/thumbs/0211.jpg
date: Sun, 20 May 2018 23:28:17 +0000
author: Derek Taylor
tags: ["Social Media", ""]
---

#### VIDEO

{{< amazon src="Social+Media+Causes+Mental+Illness.mp4" >}}
&nbsp;

#### SHOW NOTES

Social media is a dominating force in so many of our lives. But is it a positive or a negative force in our lives? Social media has been linked to a number of mental illnesses and is know to cause both physical and psychological ailments. 

The couple of statistics I used early in the video came from:

https://www.economist.com/graphic-detail/2018/05/18/how-heavy-use-of-social-media-is-linked-to-mental-illness
