---
title: "Find And Locate Your Files"
image: images/thumbs/0573.jpg
date: 2020-03-19T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Find+And+Locate+Your+Files.mp4" >}}
&nbsp;

#### SHOW NOTES

How do search for files or directories in your file system?  Well, you could use the find command.  You could also use the locate command.  

REFERENCED:
+ https://linux.die.net/man/1/find - man find
+ https://linux.die.net/man/1/locate - man locate