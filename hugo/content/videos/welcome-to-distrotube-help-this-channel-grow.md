---
title: "Welcome to DistroTube - Help This Channel Grow"
image: images/thumbs/0001.jpg
date: Thu, 22 Feb 2018 21:03:44 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Welcome+to+DistroTube+-+Help+This+Channel+Grow.mp4" >}}
&nbsp;

#### SHOW NOTES

An introduction to the DistroTube channel, what this channel currently covers, what I hope to do with this channel in the future, and how you can help this channel grow in quality. I appreciate each and every one of you guys. Peace!     Patreon: https://www.patreon.com/distrotube
