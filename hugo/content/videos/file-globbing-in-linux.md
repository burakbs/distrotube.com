---
title: "File Globbing In Linux"
image: images/thumbs/0555.jpg
date: 2020-02-26T12:22:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="How+To+Secure+The+Firefox+Browser.mp4" >}}
&nbsp;

#### SHOW NOTES

File globbing refers to “global” patterns that specify sets of filenames with wildcard characters. Globbing is the * and ? and some other pattern matchers you may be familiar with. An example would be doing something like the following in the Bash shell: 
`cp *.txt /Documents/text/`

REFERENCED:
+ https://www.distrotube.com/blog/file-globbing-in-linux/