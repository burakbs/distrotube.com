---
title: "Is Arch The New Ubuntu?"
image: images/thumbs/0469.jpg
date: Mon, 25 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Arch Linux", "Ubuntu", "Gaming"]
---

#### VIDEO

{{< amazon src="Is+Arch+The+New+Ubuntu.mp4" >}}
&nbsp;

#### SHOW NOTES

Is Ubuntu starting to lose mind share?  It appears to be the case.  And which Linux distro is gaining popularity due to Ubuntu's waning momentum?   Arch, of course!

#### REFERENCED:
+ ► https://boilingsteam.com/we-dont-game-on-the-same-distros-no-more/
