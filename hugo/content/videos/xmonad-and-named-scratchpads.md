---
title: "Xmonad and Named Scratchpads"
image: images/thumbs/0360.jpg
date: Sat, 02 Mar 2019 23:07:04 +0000
author: Derek Taylor
tags: ["tiling window managers", "Xmonad"]
---

#### VIDEO

{{< amazon src="Xmonad+and+Named+Scratchpads.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been re-discovering the joys of Xmonad.   One of the cool features of Xmonad is NamedScratchpads! 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fhackage.haskell.org%2Fpackage%2Fxmonad-contrib-0.15%2Fdocs%2FXMonad-Util-NamedScratchpad.html&amp;v=oInxVelAOdE&amp;event=video_description&amp;redir_token=yMs7vlaD6DbCSEII30UnhVx0KQ18MTU1MzY0MTYzMkAxNTUzNTU1MjMy" target="_blank">https://hackage.haskell.org/package/x...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles%2Ftree%2Fmaster%2F.xmonad%2F&amp;v=oInxVelAOdE&amp;event=video_description&amp;redir_token=yMs7vlaD6DbCSEII30UnhVx0KQ18MTU1MzY0MTYzMkAxNTUzNTU1MjMy" target="_blank">https://gitlab.com/dwt1/dotfiles/tree...
