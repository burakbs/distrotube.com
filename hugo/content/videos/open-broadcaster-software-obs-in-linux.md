---
title: "Open Broadcaster Software (OBS) In Linux"
image: images/thumbs/0306.jpg
date: Tue, 13 Nov 2018 00:21:54 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Open+Broadcaster+Software+OBS+In+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm going to do a quick video on OBS in Linux.  I will discuss how to 
install OBS, how to setup OBS, how to create scenes, and how to add 
filters to your microphone.  Basically, I show you how I use OBS and 
what works for me.

<a href="https://www.youtube.com/redirect?v=GCAANDp2fsE&amp;event=video_description&amp;q=https%3A%2F%2Fobsproject.com%2F&amp;redir_token=V3bDj5Rh6LlJSbXSxwnOCoo_M5l8MTU1MzQ3MzQzMEAxNTUzMzg3MDMw" rel="noreferrer noopener" target="_blank">https://obsproject.com/
