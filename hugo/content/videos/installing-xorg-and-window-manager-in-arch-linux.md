---
title: "Installing Xorg And A Window Manager In Arch Linux" 
image: images/thumbs/0706.jpg
date: 2020-09-04T12:23:40+06:00
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Installing+Xorg+And+A+Window+Manager+In+Arch+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

So on a previous video, I ran through installing the base system for Arch Linux.  But we need to go ahead and install Xorg and a window manager, because no one wants to live in a tty (unless your neckbeard has grown long).  I will play with the xinitrc and I will also show you how to enable the AUR and install Yay.