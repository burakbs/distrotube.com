---
title: "Is Elive The Best Linux Distro Ever?" 
image: images/thumbs/0713.jpg
date: 2020-09-14T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Elive"]
---

#### VIDEO

{{< amazon src="Is+Elive+The+Best+Linux+Distro+Ever.mp4" >}}
&nbsp;

#### SHOW NOTES

Elive is a Debian-based Linux distro that uses the old E16 window manager. A lightweight yet powerful distro, Elive markets itself as "maybe the best Linux OS ever made."  Does it live it to the hype?  I'm going to take a quick look at Elive 3.8.16 beta.

REFERENCED:
+ https://www.elivecd.org/