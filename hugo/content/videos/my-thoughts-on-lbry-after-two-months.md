---
title: "My Thoughts On LBRY After Two Months"
image: images/thumbs/0563.jpg
date: 2020-03-06T12:22:40+06:00
author: Derek Taylor
tags: ["LBRY", ""]
---

#### VIDEO

{{< amazon src="My+Thoughts+On+LBRY+After+Two+Months.mp4" >}}
&nbsp;

#### SHOW NOTES

Another edition of DT Live! This is a somewhat lengthy rant on my experience with LBRY.  I've been on the LBRY for two months and there are things about the platform that I really like and some things about the platform that need serious work.  Also, the good folks at LBRY recently sent me a gift. I will take a look at the daily build of UUbuntu 20.04 Focal Fossa.   May discuss some other Linux-y topics.  And I will converse with you guys hanging out in the YouTube chat!   Be there or be square.

REFERENCED:
+ https://lbry.tv/$/invite/@DistroTube:2
