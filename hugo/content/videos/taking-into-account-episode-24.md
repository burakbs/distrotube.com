---
title: "Taking Into Account, Ep. 24 - Windows Reserve Storage, Game Devs, Snap Games, GDPR, Ubuntu $, FLOSS"
image: images/thumbs/0332.jpg
date: Thu, 10 Jan 2019 20:02:50 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+24.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=37s">0:37 Future versions of Windows 10 will reserve 7GB of storage to prevent big updates from failing. 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=323s">5:23 Game developer, Jonathan Blow, reveals what it would take for him to develop for Linux. 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=655s">10:55 A look at the seven best Linux games available on the Snap Store. 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=1013s">16:53 If your privacy is in the hands of others, then you don't have any. 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=1399s">23:19 Inside Canonical's financial and what they tell us about this leading Linux company. 

<a href="https://www.youtube.com/watch?v=S9CD8DFAPKE&amp;t=1691s">28:11 I respond to questions about a recent image I posted on Youtube and Mastodon. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fbetanews.com%2F2019%2F01%2F08%2Fwindows-10-reserve-7gb-of-storage%2F" target="_blank">https://betanews.com/2019/01/08/windo...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fwww.gamerevolution.com%2Fnews%2F480649-jonathan-blow-develop-for-linux" target="_blank">https://www.gamerevolution.com/news/4...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F01%2Fbest-games-for-ubuntu-snap" target="_blank">https://www.omgubuntu.co.uk/2019/01/b...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fwww.linuxjournal.com%2Fcontent%2Fif-your-privacy-hands-others-alone-you-dont-have-any" target="_blank">https://www.linuxjournal.com/content/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Finside-ubuntus-financials%2F" target="_blank">https://www.zdnet.com/article/inside-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=S9CD8DFAPKE&amp;event=video_description&amp;redir_token=CqbfMXXM94bs6rc_RsFq1X5QW3B8MTU1MzU0NDE3OEAxNTUzNDU3Nzc4&amp;q=https%3A%2F%2Fwww.gnu.org%2Fphilosophy%2Ffloss-and-foss.en.html" target="_blank">https://www.gnu.org/philosophy/floss-...
