---
title: "My First Rice - Giving My Qtile Desktop A New Look"
image: images/thumbs/0259.jpg
date: Mon, 13 Aug 2018 19:15:26 +0000
author: Derek Taylor
tags: ["qtile", ""]
---

#### VIDEO

{{< amazon src="My+First+Rice+-+Giving+My+Qtile+Desktop+A+New+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I've been staring at the same desktop for months.  Time to "rice" my qtile desktop.   

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=QESVJBEwBFA&amp;redir_token=hk3u3jfaCBfj5N1yhkQB3RhMtxd8MTU1MzQ1NDkyOEAxNTUzMzY4NTI4&amp;q=https%3A%2F%2Fwww.reddit.com%2Fr%2Funixporn%2Fcomments%2F974frp%2Fqtile_my_workinprogress_rice%2F" target="_blank">https://www.reddit.com/r/unixporn/com...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=QESVJBEwBFA&amp;redir_token=hk3u3jfaCBfj5N1yhkQB3RhMtxd8MTU1MzQ1NDkyOEAxNTUzMzY4NTI4&amp;q=https%3A%2F%2Fterminal.sexy%2F" target="_blank">https://terminal.sexy/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=QESVJBEwBFA&amp;redir_token=hk3u3jfaCBfj5N1yhkQB3RhMtxd8MTU1MzQ1NDkyOEAxNTUzMzY4NTI4&amp;q=http%3A%2F%2Fwww.qtile.org%2F" target="_blank">http://www.qtile.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=QESVJBEwBFA&amp;redir_token=hk3u3jfaCBfj5N1yhkQB3RhMtxd8MTU1MzQ1NDkyOEAxNTUzMzY4NTI4&amp;q=https%3A%2F%2Fgitlab.com%2Fdwt1" target="_blank">https://gitlab.com/dwt1
