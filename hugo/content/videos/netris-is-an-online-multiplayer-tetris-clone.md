---
title: "Netris Is An Online Multiplayer Tetris Clone"
image: images/thumbs/0468.jpg
date: Sun, 24 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Gaming", "terminal"]
---

#### VIDEO

{{< amazon src="Netris+Is+An+Online+Multiplayer+Tetris+Clone.mp4" >}}
&nbsp;

#### SHOW NOTES

Want to play a multiplayer game of Tetris online via SSH?  I introduce you to Netris.  Just open a terminal and  enter a simple command and VOILA!  

#### REFERENCED:
+ ► https://git.sr.ht/~tslocum/netris
