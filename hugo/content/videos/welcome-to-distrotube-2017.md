---
title: "Welcome to DistroTube"
image: images/thumbs/0001.jpg
date: Tue, 10 Oct 2017 01:43:12 +0000
author: Derek Taylor
tags: ["Welcome", "Patreon"]
---

#### VIDEO

{{< amazon src="Welcome+to+DistroTube.mp4" >}}  
&nbsp;

#### SHOW NOTES

What is DistroTube? Well, this channel will be dedicated to all things linux. We will install and review new linux distro releases. We will review the latest software apps. We will also focus heavily on education. You will find tutorials on basic linux usage such as how to use the terminal and navigating through the linux directory structure and much more. Help grow this channel as I try to help grow linux awareness. Guys, please like, subscribe, and share. Also like and follow me on Mastodon. Thank you for your support!
