---
title: "Bashtop Is An Htop Alternative Written In Bash"
image: images/thumbs/0608.jpg
date: 2020-04-27T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Bashtop+Is+An+Htop+Alternative+Written+In+Bash.mp4" >}}
&nbsp;

#### SHOW NOTES

Bashtop is a resource monitor similar to htop.  It shows usage and stats for processor, memory, disks, network and processes.  It is written in more than 3,500 lines of Bash.

REFERENCED:
+ https://github.com/aristocratos/bashtop