---
title: "Day 2 - Newsboat RSS Newsfeed Reader"
image: images/thumbs/0176.jpg
date: Thu, 12 Apr 2018 22:29:56 +0000
author: Derek Taylor
tags: ["terminal", "rss"]
---

#### VIDEO

{{< amazon src="Day+2+-+Newsboat+RSS+Newsfeed+Reader.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 2 of my 30 challenge. I'm living in a tiling window manager for the next 30 days and only using terminal-based applications where possible. Loving my time so far in the Lynx browser and Neomutt email client. And I found a really nice RSS feed reader called Newsboat! 
