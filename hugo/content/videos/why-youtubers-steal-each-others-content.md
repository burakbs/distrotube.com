---
title: "'Why YouTubers Steal Each Others Content.' (And Other Questions Answered)"
image: images/thumbs/0657.jpg
date: 2020-06-30T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Why+YouTubers+Steal+Each+Others+Content+And+Other+Questions+Answered.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  Questions include my thoughts on the nano editor, alternatives to YouTube, and why so many YouTubers steal each other's content?

REFERENCED:
+ https://lbry.tv/@DistroTube:2 - DT on LBRY