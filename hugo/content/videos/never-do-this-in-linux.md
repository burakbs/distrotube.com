---
title: "Never Do This In Linux"
image: images/thumbs/0514.jpg
date: 2020-01-14T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Never+Do+This+In+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

New-to-Linux users often ask for advice on what NOT to do in Linux. The number of really bad things you do in Linux? That number is legion! But I will share seven things that you really should not do in Linux unless you have a darn good reason.

REFERENCED:
+ https://www.distrotube.com/blog/seven-things-to-avoid-on-linux/
