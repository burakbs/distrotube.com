---
title: "An Initial Look at ArcoLinuxB Bspwm Edition"
image: images/thumbs/0245.jpg
date: Tue, 17 Jul 2018 00:26:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArcoLinuxB", "bspwm"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/an-initial-look-at-arcolinuxb-bspwm/7e6634b21c73f32fe12062eb635d5c41b9cbde35?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Today, I'm taking an initial look at ArcoLinuxB Bspwm. https://arcolinux.info/
