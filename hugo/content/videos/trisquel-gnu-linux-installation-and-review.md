---
title: "Trisquel GNU/Linux 8.0 Install and Review"
image: images/thumbs/0181.jpg
date: Thu, 19 Apr 2018 22:38:20 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Trisquel"]
---

#### VIDEO

{{< amazon src="Trisquel+GNULinux+8.0+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of the recently released Trisquel Gnu/Linux 8.0 Trisquel is an entirely "free" as in "libre" Ubuntu-based distribution that is endorsed by the Free Software Foundation. <a href="https://trisquel.info/">https://trisquel.info/</a>
