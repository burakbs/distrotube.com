---
title: "Switching to GNU Emacs"
image: images/thumbs/0446.jpg
date: Fri, 04 Oct 2019 23:52:00 +0000
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Switching+to+GNU+Emacs.mp4" >}}
&nbsp;

#### SHOW NOTES

So you guys have been asking for me to take a look at GNU Emacs. Well, that day has come. I'm making the switch!

#### REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.gnu.org%2Fsoftware%2Femacs%2F&amp;v=Y8koAgkBEnM&amp;event=video_description&amp;redir_token=6zLK_oNSDzqGCz2UwortZxtX57h8MTU3NzQ5MDc1N0AxNTc3NDA0MzU3" target="_blank" rel="nofollow noopener noreferrer">https://www.gnu.org/software/emacs/
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdraculatheme.com%2Femacs%2F&amp;v=Y8koAgkBEnM&amp;event=video_description&amp;redir_token=6zLK_oNSDzqGCz2UwortZxtX57h8MTU3NzQ5MDc1N0AxNTc3NDA0MzU3" target="_blank" rel="nofollow noopener noreferrer">https://draculatheme.com/emacs/
