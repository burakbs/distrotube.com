---
title: "Pre-Recorded Production Is Bloat. Let's Livestream Instead!"
image: images/thumbs/0398.jpg
date: Mon, 20 May 2019 14:41:59 +0000
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Pre-Recorded+Production+Is+Bloat.+Let's+Livestream+Instead!-i3q4gCCDHJQ.mp4" >}}
&nbsp;

#### SHOW NOTES

Just a minimal, bloat-free livestream. I might discuss my time so far with the Awesome window manager, the ErgoDox EZ keyboard, the meaning of life, and other random thoughts. And, of course, I will interact with you guys in the YouTube chat. So come hangout for a bit!
