---
title: "Regolith Linux - Combining Ubuntu With i3"
image: images/thumbs/0412.jpg
date: Mon, 17 Jun 2019 03:40:14 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Regolith Linux"]
---

#### VIDEO

{{< amazon src="Regolith+Linux+Combining+Ubuntu+With+i3.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Regolith Linux. This is an Ubuntu-based Linux distro that uses the i3 tiling window manager combined with a full suite of software.


&nbsp;
#### REFERENCED:
<a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=http%3A%2F%2Fregolith-linux.org%2F&amp;redir_token=znN_ocsYbF59Pl3IQc2GD-mQBI18MTU2NjUzMTY1M0AxNTY2NDQ1MjUz&amp;v=B1Suas9OtqU&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">http://regolith-linux.org/
