---
title: "'Hey, DT. Does Emacs Violate Unix Philosophy?' (And Other Questions)" 
image: images/thumbs/0728.jpg
date: 2020-10-04T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", "Emacs"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/hey-dt-does-emacs-violate-unix/8607e5b173b3d7c2e085da016bdde16358877da9?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this lengthy rant video, I address a few questions and comments that I've been receiving from viewers.  I discuss what phone I use, whether Emacs violates the Unix Philosophy, why I type "clear" in my terminal, what I use to shave my "bald" head, why I am not "bald", and how I deal with professional burnout.