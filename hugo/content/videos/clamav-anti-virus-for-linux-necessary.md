---
title: "ClamAV - Anti-Virus for Linux - Is It Necessary?"
image: images/thumbs/0021.jpg
date: Tue, 24 Oct 2017 02:23:10 +0000
author: Derek Taylor
tags: ["Command Line", "ClamAV"]
---

#### VIDEO

{{< amazon src="ClamAV+-+Anti-Virus+for+Linux+-+Is+It+Necessary.mp4" >}}  
&nbsp;

#### SHOW NOTES

I review the ClamAV anti-virus software for Linux. ClaamAV is free and open source software that you run from the terminal. ClamAV - https://www.clamav.net/
