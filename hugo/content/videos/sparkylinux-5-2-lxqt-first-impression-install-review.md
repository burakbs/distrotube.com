---
title: "SparkyLinux 5.2 LXQt First Impresion Install & Review"
image: images/thumbs/0088.jpg
date: Mon, 15 Jan 2018 01:16:54 +0000
author: Derek Taylor
tags: ["Distro Reviews", "SparkyLinux", "LXQt"]
---

#### VIDEO

{{< amazon src="SparkyLinux+5.2+LXQt+First+Impresion+Install+%26+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at SparkyLinux 5.2 LXQt.  Sparky is a Debian-based distro that comes in three different flavors: stable (based on Debian Stable), rolling (based on Debian Testing) and a development branch.  I chose to install the rolling version of Sparky.  Thanks to <a href="https://www.youtube.com/user/IbanezPremium">https://www.youtube.com/user/IbanezPremium</a> for recommending SparkyLinux LXqt for review.<a href="https://sparkylinux.org/">https://sparkylinux.org/</a>    
