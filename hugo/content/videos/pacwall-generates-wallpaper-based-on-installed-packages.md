---
title: "Pacwall Generates Wallpaper Based On Your Installed Packages"
image: images/thumbs/0662.jpg
date: 2020-07-07T12:23:40+06:00
author: Derek Taylor
tags: ["CLI Apps", ""]
---

#### VIDEO

{{< amazon src="Pacwall+Generates+Wallpaper+Based+On+Your+Installed+Packages.mp4" >}}
&nbsp;

#### SHOW NOTES

Pacwall is a shell script that changes your wallpaper to the dependency graph of installed packages. You can customize the colors to your liking, and you can even add a pacman hook if you wish.  It is a great utility to have in your bag if you are a Unix "ricer".

REFERENCED:
+ https://github.com/Kharacternyk/pacwall - Pacwall 