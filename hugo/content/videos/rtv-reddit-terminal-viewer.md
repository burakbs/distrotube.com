---
title: "RTV - Reddit Terminal Viewer"
image: images/thumbs/0133.jpg
date: Mon, 05 Mar 2018 21:21:58 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="RTV+-+Reddit+Terminal+Viewer.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at terminal-based application that let's you browse, post and comment on Reddit. Licensed under the MIT license, RTV is a fast and lightweight program for interacting with Reddit. <a href="https://github.com/michael-lazar/rtv">https://github.com/michael-lazar/rtv</a>
