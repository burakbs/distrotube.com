---
title: "Live Mar 20, 2018 - Playing with AntiX and JWM"
image: images/thumbs/0152.jpg
date: Tue, 20 Mar 2018 21:45:23 +0000
author: Derek Taylor
tags: ["Live Stream", "AntiX", "jwm"]
---

#### VIDEO

{{< amazon src="Live+Mar+20%2C+2018+-+Playing+with+AntiX+and+JWM.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this live stream, I'm going to be playing around with my freshly installed AntiX 17.1. I will be streaming from within the JWM session on AntiX. I think I will try to live in JWM while I use AntiX for a few months. JWM is similar in look and feel to Openbox (which you guys know I love!). I may need some help with AntiX so if you know a bit about AntiX, feel free to chime in with suggestions and tips.
