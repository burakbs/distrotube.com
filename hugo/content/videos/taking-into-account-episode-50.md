---
title: "Taking Into Account, Ep. 50 - Windows 11, Ubuntu 32-bit, Cryptojacking, Pi 4, Trash Computers"
image: images/thumbs/0422.jpg
date: Thu, 19 Sep 2019 14:34:28 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+50.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:
+ 0:52 Is it crazy to think that Windows 11 could run on Linux?  Don't be so quick  to dismiss this.
+ 5:53 Ubuntu devs detail plan for 32 bit support in Ubuntu 19.10.
+ 9:02 New Linux cryptomining malware uncovered.   These stories are way too common now
+ 12:40 The Raspberry Pi 4 is a big step up in power, but is it good enough for everyday work?
+ 20:23 Trashed computers, laptops and phones hurt the environment.   Smartphones more than any other.
+ 25:37 Now more than ever, the Free Software Foundation needs your support!

