---
title: "The Linux Community is Toxic! Let's Patch This."
image: images/thumbs/0135.jpg
date: Wed, 07 Mar 2018 21:24:33 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+Linux+Community+is+Toxic!+Lets+Patch+This..mp4" >}}  
&nbsp;

#### SHOW NOTES

A rather lengthy rant about the Linux community, and the free and open source community in general, being toxic and divisive. Are there ways to fix this problem? Leave your toughts and suggestions in the comments. The general tenor of this video is quite negative. You have been warned.
