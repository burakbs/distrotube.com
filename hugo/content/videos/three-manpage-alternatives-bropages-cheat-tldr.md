---
title: "Three Manpage Alternatives - bropages, cheat and tldr"
image: images/thumbs/0331.jpg
date: Tue, 08 Jan 2019 20:01:32 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Three+Manpage+Alternatives+-+bropages%2C+cheat+and+tldr.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the questions that I've been getting recently concerns  alternatives to manpages.  So I thought that I would take a quick look  at three manpage alternatives: bropages, cheat and tldr. 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=jHAIKzdOH1Q&amp;redir_token=4mQ8ivqh3dLXg7vsQ3w2gL_tCzF8MTU1MzU0NDA5N0AxNTUzNDU3Njk3&amp;q=http%3A%2F%2Fbropages.org%2F" target="_blank">http://bropages.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=jHAIKzdOH1Q&amp;redir_token=4mQ8ivqh3dLXg7vsQ3w2gL_tCzF8MTU1MzU0NDA5N0AxNTUzNDU3Njk3&amp;q=https%3A%2F%2Fgithub.com%2Fchrisallenlane%2Fcheat" target="_blank">https://github.com/chrisallenlane/cheat

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=jHAIKzdOH1Q&amp;redir_token=4mQ8ivqh3dLXg7vsQ3w2gL_tCzF8MTU1MzU0NDA5N0AxNTUzNDU3Njk3&amp;q=https%3A%2F%2Fgithub.com%2Ftldr-pages%2Ftldr" target="_blank">https://github.com/tldr-pages/tldr
