---
title: "The Magit Git Client Is The 'Killer Feature' In Emacs"
image: images/thumbs/0665.jpg
date: 2020-07-10T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="The+Magit+Git+Client+Is+The+Killer+Feature+In+Emacs.mp4" >}}
&nbsp;

#### SHOW NOTES

Users of other text editors often ask "why should I switch to Emacs?" or, more specifically, "what is the killer feature that Emacs offers?"  Depending on your workflow, the killer feature for Emacs could be a number of things, one them being Magit!

REFERENCED:
+ https://www.gnu.org/software/emacs/ - GNU Emacs
+ https://github.com/hlissner/doom-emacs - Doom Emacs
+ https://magit.vc/ - Magit