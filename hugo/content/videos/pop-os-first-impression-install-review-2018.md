---
title: "Pop!_OS First Impression Install & Review"
image: images/thumbs/0085.jpg
date: Wed, 10 Jan 2018 01:13:01 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Pop OS", "GNOME"]
---

#### VIDEO

{{< amazon src="Pop!_OS+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at Pop!_OS, an Ubuntu-based distro that is created by the good folks at System76. It uses the GNOME desktop environment and is primarily aimed for folks running System76 hardware. <a href="https://system76.com/pop">https://system76.com/pop</a>
