---
title: "Peppermint OS 9 Installation and First Look"
image: images/thumbs/0234.jpg
date: Sat, 23 Jun 2018 00:11:09 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Peppermint OS"]
---

#### VIDEO

{{< amazon src="Peppermint+OS+9+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a quick look at the recently released Peppermint OS 9. https://peppermintos.com/
