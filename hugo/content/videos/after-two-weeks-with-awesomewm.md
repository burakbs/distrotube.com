---
title: "After Two Weeks With AwesomeWM..."
image: images/thumbs/0400.jpg
date: Wed, 22 May 2019 14:46:03 +0000
author: Derek Taylor
tags: ["tiling window managers", "awesome"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/after-two-weeks-with-awesomewm/f528ca690b6ffb25d4054663e6168e3ed2862844?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

After two weeks on AwesomeWM, how a I getting along with it? What have I changed, especially now that I am using the ErgoDox EZ keyboard. Also, I've been incorporating a bit more dmenu in my life.


&nbsp;
#### REFERENCED:
+ https://awesomewm.org/
