---
title: "Reflecting On My Linux Journey And Where It May Lead"
image: images/thumbs/0685.jpg
date: 2020-08-02T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Reflecting+On+My+Linux+Journey.mp4" >}}
&nbsp;

#### SHOW NOTES

I ramble a bit about my Linux journey.  Well, not just my Linux journey since my story begins before Linux existed.  And even in the parts of the story that involve my Linux years, the story is really more about my journey with "free and open source software".