---
title: "Taking Into Account, Ep. 6 - Commons Clause, Microsoft Profits, Chromebooks, Bash Aliases, Joplin"
image: images/thumbs/0270.jpg
date: Thu, 30 Aug 2018 18:18:15 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+6+-+Commons+Clause%2C+Microsoft+Profits%2C+Chromebooks%2C+Bash+Aliases%2C+Joplin.mp4" >}}
&nbsp;

#### SHOW NOTES

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=40s">0:40 The Commons Clause may be the end of Open Source 

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=810s">13:30 Microsoft accounts for 1 of every 5 pounds spent on software in UK. 

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=1133s">18:53 Steam Play provides a path forward for gaming on Chromebooks. 

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=1474s">24:34 Using command line aliases saves you time and makes you productive. 

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=1830s">30:30 Joplin is a cross-platform, open source note taking and to-do application. 

<a href="https://www.youtube.com/watch?v=hlaO5BZZDZM&amp;t=2276s">37:56 I read a question from Mastodon. 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=http%3A%2F%2Fwww.consortiuminfo.org%2Fstandardsblog%2Farticles%2Fcommons-clause-helpful-new-tool-or-end-open-source-we-know-it&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">http://www.consortiuminfo.org/standar...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fcommonsclause.com%2F&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://commonsclause.com/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fopensource.org%2Fosd-annotated&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://opensource.org/osd-annotated

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fwww.theregister.co.uk%2F2018%2F08%2F24%2Fwinners_and_losers_in_the_uk_enterprise_software_rankings%2F&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://www.theregister.co.uk/2018/08...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fwww.theregister.co.uk%2F2018%2F07%2F19%2Fmicrosoft_huge_2018_q4%2F&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://www.theregister.co.uk/2018/07...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fchromeunboxed.com%2Fnews%2Fchromebook-gaming-steam-support-windows-games-linux%2F&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://chromeunboxed.com/news/chrome...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2018%2F08%2F27%2Fsteam-for-linux-adds-1000-perfectly-playable-windows-games-in-under-a-week%2F%234d0474dc55ae&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fopensource.com%2Farticle%2F18%2F8%2Ftime-saving-command-line-aliases&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://opensource.com/article/18/8/t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=t7JBppl7ouypOtHDbAMpKEKQunp8MTU1MzQ1MTU2NkAxNTUzMzY1MTY2&amp;q=https%3A%2F%2Fwww.linuxuprising.com%2F2018%2F08%2Fjoplin-encrypted-open-source-note.html&amp;v=hlaO5BZZDZM&amp;event=video_description" target="_blank">https://www.linuxuprising.com/2018/08...
