---
title: "Founder Of Open Source Is Banned By Open Source"
image: images/thumbs/0560.jpg
date: 2020-03-02T12:22:40+06:00
author: Derek Taylor
tags: ["FOSS Philosophy", ""]
---

#### VIDEO

{{< amazon src="Founder+Of+Open+Source+Is+Banned+By+Open+Source.mp4" >}}
&nbsp;

#### SHOW NOTES

One of the founders of the open source movement, a man who founded the Open Source Initiative, a man who served as the President of the OSI, a man who helped write the Open Source Definition--that man has been banned from the OSI mailing list for defending open source from attack from political activists.

REFERENCED:
+ https://opensource.org/osd-annotated - Open Source Definition
+ http://esr.ibiblio.org/?p=8609 - The Right To Be Rude
