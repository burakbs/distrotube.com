---
title: "Installing Arch Linux in Virtualbox - DT LIVE"
image: images/thumbs/0205.jpg
date: Mon, 14 May 2018 23:11:47 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Arch Linux", "Live Stream"]
---

#### VIDEO

{{< amazon src="Installing+Arch+Linux+in+Virtualbox+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Lost some of my virtual machines during all the distrohopping I've done in the last couple of months. I lost my Arch VMs so I was going to do an Arch install tonight. Why not do it live? What could go wrong?
