---
title: "TAILS, Tor and the Dark Web - DT LIVE"
image: images/thumbs/0212.jpg
date: Sun, 20 May 2018 23:29:23 +0000
author: Derek Taylor
tags: ["Distro Reviews", "TAILS", "Live Stream"]
---

#### VIDEO

{{< amazon src="TAILS%2C+Tor+and+the+Dark+Web+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm going to check out a live Linux distribution called TAILS. It is built for one purpose--anonymity! TAILS uses the Tor Browser to surf the web anonymously and to explore the "dark web." 

https://tails.boum.org/

https://www.torproject.org/
