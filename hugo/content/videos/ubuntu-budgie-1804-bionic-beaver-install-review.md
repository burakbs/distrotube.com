---
title: "Ubuntu Budgie 18.04 'Bionic Beaver' Install and Review"
image: images/thumbs/0186.jpg
date: Tue, 24 Apr 2018 22:46:31 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu Budgie"]
---

#### VIDEO

{{< amazon src="Ubuntu+Budgie+18.04+Bionic+Beaver+Install+and+Review.mp4" >}}
&nbsp;

#### SHOW NOTES

It's release week for the various flavors of Ubuntu 18.04. Today, I'm looking at Ubuntu Budgie 18.04 "Bionic Beaver". The official release date is still two days away so this is technically a beta version but nothing should change between now and the release. https://ubuntubudgie.org/
