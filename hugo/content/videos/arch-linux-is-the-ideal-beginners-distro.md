---
title: "Arch Linux Is The Ideal Beginner's Distro"
image: images/thumbs/0454.jpg
date: Fri, 01 Nov 2019 00:05:00 +0000
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/arch-linux-is-the-ideal-beginner-s/c7248b4ee4cc370c230abe9326b5c6fdb6119e5c?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I've gone back and forth on this for awhile, but I've come to the conclusion that not only is Arch easy enough for the beginner to install, Arch is the ideal beginner's distro.

#### REFERENCED:
+ https://www.archlinux.org/
