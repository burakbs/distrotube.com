---
title: "Free and Open Source Licenses"
image: images/thumbs/0526.jpg
date: 2020-01-26T12:22:40+06:00
author: Derek Taylor
tags: ["Open Source", "Free Software"]
---

#### VIDEO

{{< amazon src="Free+and+Open+Source+Licenses.mp4" >}}
&nbsp;

#### SHOW NOTES

I briefly describe four popular "free" licenses: the MIT license, BSD license, Apache license and GPL.  I discuss the difference between the "permissive" licenses and the "copyleft" licenses and why I think the permissive licenses betray the spirit of the free software movement and the open source movement.

REFERENCED:
+ https://choosealicense.com/ - Choose A License