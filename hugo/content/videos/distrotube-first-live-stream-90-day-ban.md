---
title: "My First Stream - YouTube Community Guideline Strike - 90 Day Ban"
image: images/thumbs/0097.jpg
date: Thu, 25 Jan 2018 01:24:32 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="My+First+Stream+-+YouTube+Community+Guideline+Strike+-+90+Day+Ban.mp4" >}}
&nbsp;

#### SHOW NOTES

I was given a strike by YouTube on my very first live stream.  Apparently, they do not like the "test stream" feature in OBS.  Do I have a way to appeal?
