---
title: "Chat With Patrons (November 30, 2019)"
image: images/thumbs/0494.jpg
date: 2019-11-30T12:22:40+06:00
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(November+30%2C+2019)-FIPIOOUs710.mp4" >}}
&nbsp;

#### SHOW NOTES

This Video Chat will be for my Patrons!  Patrons can join the video call via Zoom which is available on Linux, Mac and Windows. For those wishing to join the chat but are not a Patron, consider joining my Patreon ( https://www.patreon.com/distrotube ). I will post a link to the Zoom chat on my Patreon page a few minutes prior to the stream.  Hope to see you guys there!

REFERENCED:
+ ► https://www.patreon.com/distrotube  - My Patreon
