---
title: "Taking Into Account, Ep. 11 - MS loves Linux, Ubuntu kernel patching, System76 Thelio, Steam survey"
image: images/thumbs/0291.jpg
date: Thu, 04 Oct 2018 19:51:50 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+11.mp4" >}}
&nbsp;

#### SHOW NOTES

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=38s">0:38 Microsoft's love affair with Linux.  It was inevitable! 

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=518s">8:38 Google Project Zero says Debian and Ubuntu put users at risk with slow kernel patching. 

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=781s">13:01 These 21 applications let you move easily between Windows and Linux. 

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=1255s">20:55 Soon you will be able to pre-order System76's open-source Thelio computer. 

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=1556s">25:56 September saw an increase in Steam Linux gaming marketshare. 

<a href="https://www.youtube.com/watch?v=RMcrEYR11K8&amp;t=1784s">29:44 I read an email from a viewer concerned about me and "burnout." 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=BM_BI0ftP-DK-0E8MTHb_Zw3XW58MTU1MzQ1NzE1N0AxNTUzMzcwNzU3&amp;q=https%3A%2F%2Fwww.techrepublic.com%2Farticle%2Fwhy-microsofts-linux-love-affair-was-inevitable%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.techrepublic.com/article/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=BM_BI0ftP-DK-0E8MTHb_Zw3XW58MTU1MzQ1NzE1N0AxNTUzMzcwNzU3&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fmicrosoft-open-sources-ms-dos-again-this-time-on-github%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.zdnet.com/article/microso...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=BM_BI0ftP-DK-0E8MTHb_Zw3XW58MTU1MzQ1NzE1N0AxNTUzMzcwNzU3&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinux-now-dominates-azure%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.zdnet.com/article/linux-n...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=BM_BI0ftP-DK-0E8MTHb_Zw3XW58MTU1MzQ1NzE1N0AxNTUzMzcwNzU3&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fgoogle-project-zero-to-linux-distros-your-sluggish-kernel-patching-puts-users-at-risk%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.zdnet.com/article/google-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fthreatpost.com%2Fanother-linux-kernel-bug-surfaces-allowing-root-access%2F137800%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://threatpost.com/another-linux-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fwww.zdnet.com%2Fpictures%2Flinux-survival-guide-these-21-applications-let-you-move-easily-between-linux-and-windows%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.zdnet.com/pictures/linux-...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fbetanews.com%2F2018%2F09%2F27%2Fsystem76-linux-open-source-thelio%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://betanews.com/2018/09/27/syste...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fthel.io%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://thel.io/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Dnews_item%26px%3DSteam-September-2018&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?redir_token=Jpf7nuhBNKxKi4dxg2bWCsgxRfV8MTU1MzQ1NzE1OEAxNTUzMzcwNzU4&amp;q=https%3A%2F%2Fwww.vg247.com%2F2018%2F10%2F02%2Fsteam-september-survey-nvidia-windows-10-1080p%2F&amp;v=RMcrEYR11K8&amp;event=video_description" target="_blank">https://www.vg247.com/2018/10/02/stea...
