---
title: "Ubuntu MATE 17.10 Artful Aarvark - Install and Review"
image: images/thumbs/0017.jpg
date: Thu, 19 Oct 2017 02:02:48 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu MATE", "MATE"]
---

#### VIDEO

{{< amazon src="Ubuntu+MATE+17.10+Artful+Aarvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I install and do a quick review of the newly released Ubuntu MATE 17.10 Artful Aadvark. Ubuntu MATE is quickly becoming one of the most popular Linux distros out there and for good reason. It is a well-polished desktop distribution that is reminiscent of the old GNOME 2 desktop many years ago.
