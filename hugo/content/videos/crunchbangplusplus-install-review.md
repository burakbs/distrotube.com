---
title: "CrunchBangPlusPlus (#!++) Install & Review"
image: images/thumbs/0060.jpg
date: Wed, 13 Dec 2017 15:16:55 +0000
author: Derek Taylor
tags: ["Distro Reviews", "CrunchBangPlusPlus", "Openbox"]
---

#### VIDEO

{{< amazon src="CrunchBangPlusPlus+(%23!%2B%2B)+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

CrunchBangPlusPlus is a fork and a continuation of the old CrunchBang Linux distro that was popular a number of years ago. CrunchBangPlusPlus is based on Debian 9 Stretch and uses the Openbox window manager, the Tint2 panel, and the Conky system monitor. <a href="https://crunchbangplusplus.org/">https://crunchbangplusplus.org/</a>
