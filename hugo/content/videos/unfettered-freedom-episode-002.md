---
title: "Unfettered Freedom, Ep. 2 - FSF Elects President, Emacs 27.1, Mozilla layoffs, HBO Drops Linux"
image: images/thumbs/0692.jpg
date: 2020-08-12T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+2+FSF+elects+President+Emacs+271+Mozilla+layoffs.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 1:27 - Free Software Foundation finally elects a new president.
+ 6:59 - GNU Emacs 27.1 released with so many new features.
+ 9:13 - Mozilla lays off 250 employees.
+ 13:17 - HBO Max drops Linux support in all browsers.
+ 17:23 - Distro Hopping Do's and Dont's.
+ 24:46 - I hate iTunes...but I love my Patrons! 

&nbsp;
##### AUDIO VERSION OF THIS PODCAST:
+ https://www.buzzsprout.com/1263722/ep...
+ https://open.spotify.com/episode/6S7V...

&nbsp;
##### RSS FEED FOR UNFETTERED FREEDOM:
https://feeds.buzzsprout.com/1263722.rss

&nbsp;
##### MUSIC ATTRIBUTION:
"Key To Your Heart" by The Mini Vandals (from the YT Audio Library)

##### CREATIVE COMMONS LICENSE:
This video is licensed with a Creative Commons CC BY license.  By marking this original video with a Creative Commons license, I am granting the community the right to reuse and edit that video.  Freedom, baby!
