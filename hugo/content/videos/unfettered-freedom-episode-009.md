---
title: "Unfettered Freedom, Ep. 9 - OpenOffice, Non-Free JavaScript, Linux Journal, Planned Obsolescence" 
image: images/thumbs/0732.jpg
date: 2020-10-14T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/unfettered-freedom-ep-9-openoffice-non/e1c09db07a8e9245b45ea6699c4de3162b8b4068?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 1:32 - An open letter to Apache OpenOffice.
+ 7:44 - Free verus non-free JavaScript.
+ 15:04 - The Linux Journal is back.
+ 19:31 - Linux prevents planned obsolescence.
+ 25:48 - Linux 5.9 is released.
+ 29:20 - Outro and a THANK YOU to the patrons!