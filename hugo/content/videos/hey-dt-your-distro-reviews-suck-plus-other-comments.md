---
title: "'Hey, DT. Your Distro Reviews Suck!'' (Plus Other Comments I Get)"
image: images/thumbs/0700.jpg
date: 2020-08-23T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

{{< amazon src="Hey+DT+Your+Distro+Reviews+Suck+Plus+Other+Comments.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  I discuss my thoughts on why my distro reviews suck, Brave being the best browser, whether I am a Buddhist, AAA games and the Free Software Movement, why my videos are monetized, and why I started making videos in the first place.