---
title: "Doom Emacs - Getting Started"
image: images/thumbs/0479.jpg
date: Wed, 18 Dec 2019 00:20:00 +0000
author: Derek Taylor
tags: ["Emacs", ""]
---

#### VIDEO

{{< amazon src="Doom+Emacs+Getting+Started.mp4" >}}
&nbsp;

#### SHOW NOTES

So I'm glad I stuck with playing with standard GNU Emacs for a few weeks.  I think it was beneficial to learn the default keybindings and see how Emacs can be built from the ground up.  But I need to move back to using the Vim keybindings to save my sanity.  So I'm checking out a distribution of Emacs called Doom Emacs.  It uses the Vim bindings and includes a nice, sensible configuration that is easy to modify.
	
#### REFERENCED:
+ ► https://github.com/hlissner/doom-emacs
